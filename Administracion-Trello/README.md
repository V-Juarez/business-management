<h1>Administración de Actividades con Trello</h1>

<h3>Ricardo Barra</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción a Trello](#1-introducción-a-trello)
  - [Bienvenida y presentación](#bienvenida-y-presentación)
  - [Creación de cuenta y conocimiento de interfaz](#creación-de-cuenta-y-conocimiento-de-interfaz)
    - [CREACIÓN DE LA CUENTA](#creación-de-la-cuenta)
    - [INTERFAZ](#interfaz)
  - [Reto 1](#reto-1)
- [2. Creación de equipos, etiquetas y tareas](#2-creación-de-equipos-etiquetas-y-tareas)
  - [Añadir miembros](#añadir-miembros)
    - [Integración con Slack](#integración-con-slack)
    - [Desde Trello](#desde-trello)
    - [Desde Slack](#desde-slack)
  - [Etiquetas y tareas](#etiquetas-y-tareas)
  - [Reto 2](#reto-2)
- [3. Gestión de archivos, enlaces e imágenes](#3-gestión-de-archivos-enlaces-e-imágenes)
  - [Archivos y enlaces](#archivos-y-enlaces)
  - [Reto 3](#reto-3)
- [4. Comunicación, notificaciones y seguimiento de conversaciones](#4-comunicación-notificaciones-y-seguimiento-de-conversaciones)
  - [Comunicación, notificaciones y seguimiento](#comunicación-notificaciones-y-seguimiento)
  - [Administración de notificaciones](#administración-de-notificaciones)
  - [Reto 4](#reto-4)
- [5. Herramientas avanzadas](#5-herramientas-avanzadas)
  - [Mover, copiar y compartir tarjetas](#mover-copiar-y-compartir-tarjetas)
  - [Definir fechas de vencimiento y activar el calendario](#definir-fechas-de-vencimiento-y-activar-el-calendario)
  - [Archivar y restaurar elementos](#archivar-y-restaurar-elementos)
  - [Vinculación de tarjetas y tableros](#vinculación-de-tarjetas-y-tableros)
  - [Reto 5](#reto-5)
- [6. Conectando y extendiendo Trello con otras herramientas](#6-conectando-y-extendiendo-trello-con-otras-herramientas)
  - [Atajos de teclado](#atajos-de-teclado)
  - [Power-Ups y Google Calendar](#power-ups-y-google-calendar)
  - [Integración de Trello con Slack](#integración-de-trello-con-slack)
  - [Otras herramientas de gestión de tiempo y tareas](#otras-herramientas-de-gestión-de-tiempo-y-tareas)
  - [Reto 6](#reto-6)
  - [Conclusiones](#conclusiones)

# 1. Introducción a Trello

## Bienvenida y presentación

**Trello** es una herramienta digital basada en la metodología ágil llamada KanBan, que mejora la visibilidad del flujo de trabajo mediante un tablero.

Es muy popular y comúnmente utilizada para el seguimiento de proyectos **Open Source** y eventos.

> Plus for Trello, es una extensión de Chrome para trello, donde puedes realizar adelante proyecto con scrum: gestionar equipos, registrar horas, burndown chart, entre otros.

## Creación de cuenta y conocimiento de interfaz

Manejo de listas:

• **IDEAS:** Aquellas ideas o comentarios interesantes que se pueden convertir en realidad más adelante.
• **TO DO:** Todas aquellas tareas que están para desarrollar.
• **DOING:** Aquellas tareas que ya están en proceso de realización.
• **SPRINT:** Aquellos deberes que se presentan de último momento y merecen de atención URGENTE.
• **COMPLETE:** Las tareas que están finalizadas, pero que no han sido validadas por el líder de equipo o de proyecto
• **VALIDATED:** Tareas terminadas y validadas.

### CREACIÓN DE LA CUENTA

• Entramos a trello.com
• Registramos nuestros datos.
• Verificamos en el correo.
• Personalizar el perfil.

### INTERFAZ

• Presentación de la Home Pages
• Tableros >> Crear nuevo tablero >> Darle un nombre >> Color >> Privado/público
• Ordenar las listas: (Ideas, to do, doing, sprint, complete, validated)
• Añadimos las tarjetas de tareas

## Reto 1

Realiza el siguiente reto para poner en práctica tus conocimientos:

  1. Crea una cuenta en Trello y configura el espacio de trabajo considerando tableros, listas y tarjetas.

  2. Explica la anatomía y funcionamiento general de la herramienta en los comentarios.

- **Tableros:** Estos son como carpetas de cosas por hacer donde se pueden visualizar las listas.
- **Listas:** Contienen un conjunto de tareas y son parte de un tablero.
- **Tarjetas:** Son las listas de tareas que deseamos realizar y monitorear.

1. **Tableros**

Menú en el que esta disponible la búsqueda de otro tableros y creaciones de estos mismo como ver los tableros ya concluidos.

<img src="https://i.ibb.co/TwnGkG7/Trello1.jpg" alt="Trello1" border="0">

2. **Barra de búsqueda**

Buscador que permite buscar por medio de operadores encontrar tarjetas, comentarios, checklist, etc.

<img src="https://i.ibb.co/mD8cCPt/Trello2.jpg" alt="Trello2" border="0">

3. **Tablero**

Permite crear múltiples listas y dentro de estas mismas añadir tarjetas que pueden contener checklist, comentarios, descripción,etc.

<img src="https://i.ibb.co/vQ7NYtG/Trello3.jpg" alt="Trello3" border="0">

4. **Lista**

Parte fundamental del tablero ya que en ellas nos permite crean nuestros propias tarjetas para el uso personal o para gestionar proyectos dentro de un grupo de trabajo o desarrollo.

<img src="https://i.ibb.co/RCqwRW9/Trello4.jpg" alt="Trello4" border="0">

5. **Tarjeta**

Parte de las lista que sirve para administrar las tareas necesarias, actividades, lista de compra, entre otras cosas.

<img src="https://i.ibb.co/zxGtVJJ/Trello5.jpg" alt="Trello5" border="0">

# 2. Creación de equipos, etiquetas y tareas

## Añadir miembros

Una de las funciones mas básicas y necesarias de Trello es la creación de Equipos, con los que podras gestionar mejor los múltiples tableros que sean creados y los plugins tan útiles en esta plataforma

### Integración con Slack

Existen dos maneras. Una desde Trello y otra desde Slack.

### Desde Trello

  1. Cree un canal específico en Slack para este tablero, con los integrantes del mismo.
  2. En el tablero de Trello clic en “Menú - Power Ups”
  3. Buscar “Slack”. Añadir.
  4. Clic en la esquina superior derecha del Power-Up y configurar.
  5. Seleccionar Equipo, canal y las alertas que se requiera.
  6. En el tablero clic en “Invitar” luego en enlace.
  7. Copiar enlace en el canal de Slack y esperar que los miembros se suscriban.

Listo. Se emitirá una alerta según la configuración definida en dicho canal.

### Desde Slack

  1. Clic en el nombre del Team - Administración - Gestionar Aplicaciones.
  3. Buscar la aplicación “Trello” y “Alertas de Trello”
  4. En “alertas de Trello” crear una nueva configuración.
  5. Configurar el canal y las alertas necesarias.

Listo, ya podemos visualizar los cambios del tablero en dicho canal de Slack.

- [Trello - guía para crear equipos](https://trello.com/guide/onboard-team)
- [Trello - guía para administrar equipos](https://trello.com/guide/team-administration)

## Etiquetas y tareas

A las tarjetas creadas se le pueden asignar a las personas del equipo, indicando que esa persona es quien está trabajando en ella y recibiendo información de cada actualización de esa tarjeta.

Las etiquetas aparecen en la propia tarjeta como indicadores de colores, a las que tu das el valor que quieras. En este caso se asigna a la etiqueta verde el valor de prioridad baja y a la etiqueta roja la de prioridad alta. Así podremos a llegar a distinguir varias categorias en una misma lista.
Clickeando en la etiqueta aparecerá el nombre que le has dado.

Las checklist sirven para hacer un seguimiento del progreso de varias tareas cortas relacionadas con una tarea mayor.

Se pueden ocultar las tareas ya realizadas para que no molesten en la Checklist.
También se pueden convertir en tarjetas si necesitan tener mas importancia y mas carga de trabajo que la esperada.

## Reto 2

Realiza el siguiente reto para poner en práctica tus conocimientos:

  - Crea un equipo de trabajo y asociarlo a una tarjeta en particular.

  - Asigna colores y nombres a distintas etiquetas dentro de una tarjeta.

  - Genera una nueva lista de tareas basada en una ya existente.

Las etiquetas de Trello las implementé guiándome en las prioridades del curso Gestión efectiva del Tiempo asignándo los colores de las etiquetas según la prioridad 1-4 siendo:

- **URGENTE / NO IMPORTANTE:**

Aunque no son urgentes deberían ser las primeras a realizar para salir rápido de ellas.

- **URGENTE / IMPORTANTE:**

Es prudente analizar el por qué un tema que es importante, llegó a convertirse en urgente. Quizá tuvieron poca planeación y no re ejecutaron en su momento con mayor tiempo.

- **NO URGENTE / NO IMPORTANTE:**

No se deberían realizar estas tareas. Son actividades que nos disipan del objetivo. Quizá sean divertidas, amenas. Pero hacen que se pierda el norte.

- **NO URGENTE / IMPORTANTE:**

Etapa de planificación para cumplir objetivos y metas con los tiempos adecuados para desarrollar las actividades.

<img src="https://i.ibb.co/NsJwyvG/Equipo-De-Trabajo.jpg" alt="Equipo-De-Trabajo" border="0">

# 3. Gestión de archivos, enlaces e imágenes

## Archivos y enlaces

Puedes arrastrar la imagen o cualquier documento al centro de la tarjeta y se adjuntará de manera mucho más fácil.

**Trello** permite importar imagenes y todo tipo de archivos desde el ordenador, *Drive*, *Box*, *One Drive*, *Dropbox*; includo desde otros tableros de Trello.

Tambien puedes introducir un link externo para que redireccione a esa web.
Para importar imagenes desde internet tenemos dos formas:
Copiar la dirección de enlace, que nos redireccionará a esa imagen
Copiar dirección de imagen, que cargará en portada.

Si subes una fotografia en jpg, png y similares la foto se vuelve portada de esa tarjeta automaticamente, pudiendo eliminarla de portada desde dentro.

- En las tarjetas es posible incorporar imágenes, plazos de entrega, listas de tareas, etiquetas separadas por color y comentarios asociados a cada tarea.

- En palabras simples, el tablero es la página que contiene todas las listas asociadas a un proyecto en específico, que pueden[ ser añadidas o archivadas](**Fuente: https://bit.ly/2JZ3F23**).

  [![Screenshot-3.jpg](https://i.postimg.cc/0j4m2VYP/Screenshot-3.jpg)](https://postimg.cc/H8Qj64vh)

## Reto 3

Realiza el siguiente reto para poner en práctica tus conocimientos:

- Nombra los tipos de archivo que se pueden integrar.

  Los archivos de adjuntan desde:

  - Ordenador (.jpeg, .doc, .xlsx, etc)
  - Trello (desde el mismo tablero o desde otros tableros)
  - Doogle Drive
  - Dropbox

- Asigna una imagen como portada de una tarjeta.

  [![trello-reto3.jpg](https://i.postimg.cc/LssxXbfZ/trello-reto3.jpg)](https://postimg.cc/zy9C2pxJ)

# 4. Comunicación, notificaciones y seguimiento de conversaciones

## Comunicación, notificaciones y seguimiento

*La comunicación entre los diferentes miembros del equipo es una de las cosas mas importantes para comenzar con la transformación digital en una empresa. Por eso utilizar esta herramienta para reducir los correos diarios que se envian entre miembros del equipo es muy util para reducir el tiempo que invertimos en ello.*

En las tarjetas nos **comunicaremos** mediante comentarios dentro de las tarjetas.
Cada comentario notifica a los que estén dentro de esa tarjeta de que es nuevo, para aquellas personas que no estén dentro de esa tarjeta pero querramos que les llegue el comentario necesitamos mencionarlas con **@sunombre.**

Si no eres parte de la tarjeta pero quieres estar siguiendo la tarjeta y ver las notificaciones, podemos entrar en la tarjeta y darle a ***Seguir***, haciendo que nos lleguen todas las notificaciones de esta.
Podemos hacer esto para tarjeta, lista y tablero.

> Puedes hacer seguimiento de una tarjeta, lista o tablero en particular.

Con notificaciones en tiempo real que agil flujo de informacion esto ayuda a una respuesta mas rapida ante errores o atrasos de tener solucion y equipos con responsabilidad

## Administración de notificaciones

Las notificaciones ofrecen una alternativa rapida de atender solicitudes del grupo de trabajo, pero lo mas recomendable es usarlas directamente en la herramienta para evitar sobrecarga en la bandeja de entrada.

Los filtros que se me ocurren son:

- Filtrar por leídas y no leídas (al igual que el contenido de este video)

- Filtrar por menciones (igual que twitter)

  > Nunca enviar notificaciones al correo electrónico.

## Reto 4

Realiza el siguiente reto para poner en práctica tus conocimientos:

- **Describe la forma de agregar un comentario en una tarjeta.**

  ​	Entrar a la tarjeta y en la parte inferior donde esta la caja de comentarios ahi ponerlo.

- **Haz un comentario dirigido a alguien que no forma parte de la tarjeta.**

  ​	Si necesitamos citar a alguien que no este asignado a esta tarjeta se puede hacer con un @usuario, siempre y cuando si este dentro del tablero el usuario.

- **Explica cómo hacer un seguimiento de la actividad de una tarjeta, lista o tablero sin ser miembro.**

  ​	Lista - en los tres puntos y despues en seguir
  Tablero - Menu - mas… y despues en seguir
  Tarjeta - Entrar a la tarjeta y despues en Acciones clic en seguir

# 5. Herramientas avanzadas

## Mover, copiar y compartir tarjetas

Herramienta, para añadir un comentario desde el correo electrónico,

- [Trello | Tableros publicos](https://trello.com/b/mSToXF5L/trello-slack-google-tips-tricks)

Optimizar tiempo es una premisa de la transformación digital, estas opciones de administrar tarjetas (Mover, copiar, compatir) permite, aprovechar recursos ya creados, incluir la tarjeta en una paginca como código HTML y administrar las actividades de un plan de trabajo complejo.

Que interesante el hecho de poder enviar comentarios por coreo a tarjetas especificas, la debilidad que le veo es que uno debiese tener una lista de correos por tarjeta que tiene en cada tablero, quiero verle la utilidad a esto, me parece muy interesante, paro los fines prácticos no los veo claros

## Definir fechas de vencimiento y activar el calendario

**Las fechas de vencimiento en Trello** nos permiten definir los DeadLines (DL) de nuestras tareas. Aparece

Trello nos recuerda la proximidad del vencimiento con un color:
**Naranja** si la fecha actual es muy próxima a la colocada,
**Roja** si se ha pasado de DL,
**Sin color** si todavía queda tiempo suficiente para que venza.

Para sacarle mayor funcionalidad necesitamos activar el calendario.
Necesitamos ir a Menú (arriba a la derecha), entrar en **Power-ups**, buscamos ‘Calendario’ se llama *Calendar*.

Aparecerá un calendario y aparecerán los DL que se hayan escrito.
Podremos crear tarjetas directamente en el Calendario.

Tambien podremos hacer que este calendario se sincronice con tu calendario personal para tener todo unificado.

El cronograma de actividades cuenta con muchas ventajas para la gestión, entre las que cabría destacar:

- Proporciona una base para supervisar y controlar el desarrollo de todas y cada una de las actividades que componen el proyecto.
- Ayuda a determinar la mejor manera de asignar los recursos, para que se pueda alcanzar la meta del proyecto de manera óptima.
- Facilita la evaluación de la manera en que cada retraso puede afectar a otras actividades y a los resultados finales.
- Permite averiguar dónde van a quedar recursos disponibles, de forma que se puede proceder a su reasignación a otras tareas o proyectos.
- Sirve de base para realizar un seguimiento del progreso del proyecto.
  Intentar gestionar un proyecto sin apoyarse en una herramienta como el cronograma de actividades es casi como avanzar con los ojos cerrados ya que, sin la visión y claridad que aporta la toma de decisiones pierde objetividad y el nivel de riesgo aumenta considerablemente.

La versión trello gold, la cual podemos conseguir con un link de recomendacín, nos permite hasta 3 power-ups. Les recomiendo usar Drive & Slack como herramientas asociadas a trello.

## Archivar y restaurar elementos

**Trello** se basa en la **simplificacion** y en la **optimización** del tiempo. En cuanto tenemos demasiadas tarjetas abiertas y/o terminadas se hace pesado buscar, para ello una buena utilidad que tiene Trello es **archivar y restaurar** elementos para sacarlos de vista.
**Para archivar una tarjeta** solamente tenemos que entrar en el **menú de la tarjeta** y darle a Archivar. Para desarchivarla iremos a *Menú*, *Elementos Archivados* y enviar al tablero, donde volverá al sitio original.

Tambien podremos **archivar un tablero entero**, tenemos la suerte de poder desarchivarlo dentro del Menú de los Tableros.

Archivar y Restaurar Elementos

- Trello Permite Archivar Tarjetas, Listas, tablero
- Tienes la posibilidad de desarchivar estos elementos
- Puedes eliminarlos definitivamente (Esta acción no se puede deshacer
- En cuentas Premiun puedes habilitar permisos para que solo ciertos usuarios puedan hacer ciertas acciones como archivar o eliminar

[![archivar.jpg](https://i.postimg.cc/520Vd1gh/archivar.jpg)](https://postimg.cc/F72q3M2p)

Esta función de **Trello** nos ayuda:

- Tener un tablero más organizado.
- Mejora el flujo de trabajo cuando se trabaja con clientes.
- Tienes la opción de ocultar o eliminar tableros pasados (solo si es necesario)

## Vinculación de tarjetas y tableros

Cuanto mas se complica el **proyecto con cientos de tarjetas**, es muy util usar la vinculación entre *Tarjetas y Tableros*.

Podemos vincular tarjetas entre ellas utilizando el link que se genera cuando compartes la tarjeta, para así explicar el contexto y no llenar de [texto una tarjeta innecesariamente](https://trello.com/c/glfPYhyx)

[![Tableros.jpg](https://i.postimg.cc/yYbHKxdF/Tableros.jpg)](https://postimg.cc/56LGqx80)

Todas las tarjetas y tableros tienen URLs únicas.

## Reto 5

Realiza el siguiente reto para poner en práctica tus conocimientos:

- **Explica la diferencia entre arrastrar y soltar tarjetas entre distintas listas y utilizar la función “mover” una tarjeta.**
  - La diferencia entre arrastrar y mover una tarjeta radica en que la primera facilita el trabajo haciendo uso del mouse y la podemos llevar a donde queramos, y mover una tarjeta requiere de realizar el despliegue de los menús de la tarjeta, así como de conocer la ubicación exacta a donde la queremos ubicar finalmente. Se puede jugar más con la primera opción que con la segunda.

-  **Explica las ventajas de copiar una tarjeta en vez de moverla.**
  - Si tenemos una tarjeta la cual podremos replicar en el resto de las listas, en lugar de estar escribiendo y haciendo las respectivas modificaciones a cada una, lo que hacemos es copiarla y de esta forma no trabajamos en exceso y simplemente hacemos las modificaciones que haya lugar en las nuevas tarjetas, ahorrando tiempo y trabajo.

- **Establece una fecha de vencimiento para una tarjeta específica.**

  - Abrimos la tarjeta usar y dentro de su menú se encuentra la opción “Vencimiento”, damos clic y se despliega otro menú que contiene entre otras cosas un calendario; escogemos la fecha y la hora y creamos un recordatorio que creamos prudente para recordarnos de la fecha en que vence dicha tarea. Guardamos y listo, ésta aparecerá en la tarjeta del listado correspondiente.

- **Cambia las fechas de vencimiento utilizando la vista calendario.**

  - Abrimos el calendario dando clic en su respectivo botón, ahí seleccionamos la tarjeta a cambiar de fecha y la arrastramos a la nueva fecha de vencimiento y la soltamos. Así ha quedado establecida una nueva fecha para la tarjeta.

- **Dentro de una tarjeta, genera un vínculo a otro tablero.**

  - Primero vamos al tablero a que haya que vincular; ya ahí damos clic en el menú superior derecho “mostrar menú”, seleccionamos la opción “más” y en seguida la opción “Enlazar a este tablero”, copiamos el enlace y cerramos el menú. A continuación vamos a otro tablero donde se encuentre la tarjeta en la que vamos a enlazar el tablero anterior y, abrimos la tarjeta de destino; en la sección de “Descripción” damos algunos detalles sobre lo que trata el enlace y pegamos ahí el enlace del otro tablero, guardamos y listo.

  **Nota**: Las instrucciones dadas en este post pueden diferir de las que se dan en el curso ya que la actulización de la aplicación Trello contiene algunas diferencias a la versión que uso el profesor para dar su curso.

# 6. Conectando y extendiendo Trello con otras herramientas

## Atajos de teclado

[![atajosde-Teclado.jpg](https://i.postimg.cc/FRTWM9XY/atajosde-Teclado.jpg)](https://postimg.cc/mzFyMWD4)

**[Trello | Shortcuts](https://trello.com/shortcuts)**

- Feclas ← → Para moverse entre las listas
- Fechas ↑↓ Para moverse entre las tarjetas
- Enter para entrar a la tarjeta

- L --> Labels = Etiquetas
- M --> Members = Miembros
- D -->Deadline [Fecha (Vencimiento)]
- Space bar --> Asignar automáticamente a una tarjeta
- C --> Archivar
- S --> Seguir

## Power-Ups y Google Calendar

Los Power-Ups son extensiones de Trello o aplicaciones externas que aumentan sus funcionalidades.
Para integrar el **Calendario de Trello** con *Google Calendar*, vamos a opciones del calendario, copiamos el *link proporcionado* y vamos a darle a **Agregar calendarios** en *Google Calendar*, y clickeamos agregar calendario a traves de una URL.
Tened cuidado con los husos horarios y las actualizaciones entre herramientas, Google actualiza los Feeds una vez al día.

Otra opción para ver los cambios a tiempo real: [Cronofy](https://trello.cronofy.com/)

No olvidemos eliminar la conexión con Google y Trello.

Con esta aplicación podremos ver a tiempo real los cambios en Trello.

## Integración de Trello con Slack

Una de las herramientas mas utilizadas por los programadores es [Slack](https://slack.com/).
Tenemos la posibilidad de integrar Slack a nuestro Trello.
Primero lo buscaremos en los Power-Ups, el límite de Trello es de 1 Power-Up para las cuentas gratuitas, por lo que necesitaremos deshabilitar el Calendario.

- Necesitamos conectar cada canal con cada tablero que queramos sincronizar.
- SlackBot nos avisará de las cosas creadas en Trello.
- Podemos vincular equipos enteros en Slack en la configuración de los equipos de Trello.

**¿Cuando utilizar Trello y cuando Slack?**

- Cuando se hable sobre cosas especificas de los proyectos o las tarjetas se recomienda Trello

- Utilizaremos Slack cuando necesitamos comunicaciones mas generales, como convocatorias de reuniones, eventos y similares.

  > **Tip** también podemos acudir a la app de Slack e integrar un Trello Bot para que se notifiquen las acciones que nosotros deseemos dentro de cualquier canal que necesitemos.

  **En que momento manejar la comunicación en Slack o Trello**
  Cuando se habla de cosas especificas del proyecto se debe tener la comunicación en Trello, pero cuando se revisa temas generales es mejor utilizar Slack.

## Otras herramientas de gestión de tiempo y tareas

Para los amantes del OpenSource. Aquí algunas Alternativas.

**Para Trello.**

- Wekan

- Taiga

- Kanboard

**Para Slack.**

- Rcoekt.Chat
- MatterMost

**Gestión de proyectos(Project Management)**

```
- Trello
- Asana
- Wrike
- Monday
- Basecamp
- Todoist
- Jira
```

**Gestión de Actividades Diarias**

```
- Tick Tick
- Any.do
- Microsoft to do (antes WunderList)
```

Las gestión de actividades diarias es mas para el smartphone con recordatorios de actividades del día a día, como hacer compras, ir al banco, pendientes en casa, etc.

## Reto 6

Realiza el siguiente reto para poner en práctica tus conocimientos:

- **Describe cómo se accede al listado o glosario de atajos de teclado.**

  ​	Desde https://trello.com/shortcuts.

- **Conecta Trello con Google Calendar.**

  ​	En power-ups registrados el de calendar. Obtenemos el enlace y desde Google Calendar, damos de alta un nuevo calendario con el enlace que obtuvimos en trello.

- **Menciona las principales maneras de vincular Trello con Slack.**

  ​	Desde power-ups, vinculamos la cuenta de slack. Y podemos ver los cambios desde el canal. Tambien a traves de un team, podemos vincular y seleccionar las configuraciones.

- **Nombra dos herramientas que sean alternativas a Trello.**

  ​	Monday / Wunderlist


## Conclusiones

¡Hola! Felicitaciones por haber llegado hasta aquí.

Si viste el curso completo ahora conoces a detalle la interfaz de Trello, una herramienta para optimizar la productividad cuyo objetivo es dejar de trabajar en exceso y comenzar a trabajar de manera más inteligente.

Recuerda que utilizar esta herramienta en función de tus proyectos y necesidades es lo que verdaderamente te permite evaluar tu progreso en esta área.

¡Muchos éxitos!
