<h1>Comunicación Online con Slack</h1>

<h3>Mariandrea Cruz</h3>

<h1>Tabla de Contenido</h1>

- [1. Introduccción: Slack como herramienta de comunicación](#1-introduccción-slack-como-herramienta-de-comunicación)
  - [¿Por qué usar Slack para comunicarte con tu equipo?](#por-qué-usar-slack-para-comunicarte-con-tu-equipo)
  - [Principios fundamentales de la comunicación en Slack](#principios-fundamentales-de-la-comunicación-en-slack)
- [2. Crear y configurar tu espacio de trabajo](#2-crear-y-configurar-tu-espacio-de-trabajo)
  - [Crea tu espacio de trabajo en Slack: instalación, configuración y precios](#crea-tu-espacio-de-trabajo-en-slack-instalación-configuración-y-precios)
  - [Agrega usuarios a Slack: administración de roles y permisos](#agrega-usuarios-a-slack-administración-de-roles-y-permisos)
  - [Importar o exportar tu historial en Slack](#importar-o-exportar-tu-historial-en-slack)
- [3. Dominar las funcionalidades básicas de Slack](#3-dominar-las-funcionalidades-básicas-de-slack)
  - [Qué puedes hacer en Slack: glosario de términos básicos](#qué-puedes-hacer-en-slack-glosario-de-términos-básicos)
  - [Canales de Slack: creación, uso y configuración](#canales-de-slack-creación-uso-y-configuración)
  - [Mensajes y notificaciones de Slack](#mensajes-y-notificaciones-de-slack)
  - [Emojis como forma de comunicación](#emojis-como-forma-de-comunicación)
- [4. Llevar Slack al siguiente nivel con herramientas avanzadas](#4-llevar-slack-al-siguiente-nivel-con-herramientas-avanzadas)
  - [Comparte enlaces, código y archivos en Slack](#comparte-enlaces-código-y-archivos-en-slack)
  - [Llamadas y videollamadas en Slack](#llamadas-y-videollamadas-en-slack)
  - [Configura notificaciones y recordatorios en Slack](#configura-notificaciones-y-recordatorios-en-slack)
  - [Cómo realizar búsquedas en Slack](#cómo-realizar-búsquedas-en-slack)
  - [Atajos de teclado y comandos de Slack](#atajos-de-teclado-y-comandos-de-slack)
  - [Slackbot es tu amigo: configura respuestas automáticas](#slackbot-es-tu-amigo-configura-respuestas-automáticas)
- [5. Integrar Slack con otras herramientas](#5-integrar-slack-con-otras-herramientas)
  - [Integración de Slack con Google Drive y Google Calendar](#integración-de-slack-con-google-drive-y-google-calendar)
  - [Integración de Slack con IFTTT](#integración-de-slack-con-ifttt)
- [6. Cierre del curso](#6-cierre-del-curso)
  - [Felicitaciones por completar el curso](#felicitaciones-por-completar-el-curso)


# 1. Introduccción: Slack como herramienta de comunicación

## ¿Por qué usar Slack para comunicarte con tu equipo?

Slack es una herramienta muy recomendada!

Si son programadores y quieren buena comunicación pueden integrar CodeStream con Slack y lograr comunicarse directamente desde el editor de código (Además integrar GitHub también obvio), curso de CodeStream: https://platzi.com/cursos/comunicacion-codestream/

El curso de trabajo remoto contiene un modulo de integración de herramientas como Slack.

Y para comunicación verbal o vídeo llamada el curso de Zoom.

[slidesslack.pdf](https://drive.google.com/file/d/1aYWZ6txxcr1c34AGvLlzLKCEzyt8QbDL/view?usp=sharing)

![img](https://www.google.com/s2/favicons?domain=https://open.scdn.co/cdn/images/favicon32.a19b4f5b.png)[¿Tienes lo que se necesita para ser Platzi Associate?](https://platzi.com/blog/associates/)

![img](https://www.google.com/s2/favicons?domain=https://open.scdn.co/cdn/images/favicon32.a19b4f5b.png)[Saber escribir tu propio futuro: Mariandrea Cruz - Platzi | Podcast on Spotify](https://open.spotify.com/episode/4dF65bZviN2v6gaP7iifi1)

## Principios fundamentales de la comunicación en Slack

Piensa como aplicar Slack a tu contexto y tu organizacion y como influiria a la comunicacion en tu equipo.

Principios:

- Transparencia: Slack tiene canales publicos para una comunicacion abierta y un mensaje en un canal publico sirve como medio de información para todo un equipo. Puedes buscar información ya que la información no se pierde.
- Brevedad: Evita tener lenguaje formal, la idea es que la comunicación sea rapida y clara, NO hacer SLACK BOOMBING (Mandar muchos mensajes o hacer ENTER en cada palabra).
- Relevancia: Cada canal debe alberbar un equipo, un proyecto o un tema especifico y esto debe ser claro desde el principio.
- Productividad: Tu eres dueño de tu tiempo y no de las notificaciones. Trabajo profundo (DEEP WORK), ese que necesita el 100% de nuestra concentración.
- Respeto por el tiempo de los démas: Ten conciencia cada que mencionas a un compañero o menciones al canal entero.

- Ser breve
- Ser específico
- Ser relevante
- Recordar que cada canal tiene un propósito.
- Respetar los tiempos de ´deep work´.
- Evitar hacer “Slack Bomb” (enviar múltiples mensajes que reflejan una sola idea).
- Ser conscientes de las notificaciones que generamos.

![img](https://www.google.com/s2/favicons?domain=https://artscience.ca/wp-content/uploads/2018/02/cropped-AS_glyph_RGB_blue-32x32.png)[How to Be Organized & Mindful on Slack | Art & Science](https://artscience.ca/slack-etiquette-part-1-organize-be-mindful/)

![img](https://www.google.com/s2/favicons?domain=https://artscience.ca/wp-content/uploads/2018/02/cropped-AS_glyph_RGB_blue-32x32.png)[Slack Etiquette: How to be Respectful on Slack | Art & Science](https://artscience.ca/slack-etiquette-part-2-respect-your-team-and-be-mindful/)

![img](https://www.google.com/s2/favicons?domain=https://images.hiverhq.com/images/favicon-new1.ico)[The definitive guide to Slack etiquette | Blog | Hiver™](https://hiverhq.com/blog/slack-etiquette)

![img](https://www.google.com/s2/favicons?domain=https://blog.afoolishmanifesto.com/posts/distraction-free-slack//static/img/fav.png)[Distraction Free Slack - fREW Schmidt's Foolish Manifesto](https://blog.afoolishmanifesto.com/posts/distraction-free-slack/)

# 2. Crear y configurar tu espacio de trabajo

## Crea tu espacio de trabajo en Slack: instalación, configuración y precios

lack esta disponible para los tres sistemas operativos: Windows, Mac y Linux. Muy recomendada la versión de escritorio dado que tienes las notificaciones a la mano.

Como primeros pasos:

1. Modifica tu perfil: Nombre completos, nombre de Slack, a lo que te dedicas, número de teléfono y foto de perfil.
2. Modifica tu espacio de trabajo: En ajustes. Puedes cambiar el ícono del espacio de trabajo.
   Recomienda usar siempre la aplicación.

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Get Started | Slack](https://slack.com/get-started)

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Linux | Downloads | Slack](https://slack.com/download)

## Agrega usuarios a Slack: administración de roles y permisos

- Propietario: Maximo nivel de permisos. Controla configuraciones de facturacion y seguridad.
- Administrador: Administra los miembros, los mensajes y los canales.
- Miembro: Acceso a las funcionalidades estándar.
- Invitado: Acceso restringido. Puede ser a un solo canal o a multiples canales.

**Roles administrativos**
• Propietario principal del espacio de trabajo
Todos los espacios de trabajo tienen un propietario principal, que cuenta con el nivel más elevado de permisos. Solo esta persona puede eliminar el espacio de trabajo o transferir su propiedad a otra persona.
• Propietarios del espacio de trabajo
Puede haber varias personas con este rol. Este rol tiene el mismo nivel de permisos que el propietario principal, pero no puede eliminar el espacio de trabajo ni transferir su propiedad.
• Administradores del espacio de trabajo
Puede haber varias personas con este rol. Ayudan en la administración de los miembros y canales, además de realizar otras tareas administrativas.

<img src="https://i.ibb.co/6YvhWjX/slack1.jpg" alt="slack1" border="0">

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Roles en Slack | Slack](https://slack.com/intl/es-co/help/articles/360018112273-Roles-en-Slack)

## Importar o exportar tu historial en Slack

Hacer la migración es útil cuando:

- Queremos migrar desde otros servicios o espacios de trabajo de Slack.
- Para hacer back-up.
- Si queremos integrar 2 espacios de trabajo.
- Por el cumplimiento de algún proceso que requiera esa información.

La opción de Importar / exportas datos esta en Ajustes y automatizaciones.

- La opción de exportar generará un archivo que contiene toda la información de canales públicos.
- Importar sirve para fusionar uno de esos archivos con el espacio de trabajo actual.
- Seleccionas usuarios y canales. Demora dependiendo del tamaño del archivo.

Un canal está representado por un #, y sirve para definir las conversaciones que se tendrán sobre un tema en específico, también puedes agregar a la gente que tiene interés sobre ese mismo canal. La idea es que cuando definas canales, definas también las reglas de conversación. De manera no formal te recomiendo que lo veas como si fueran grupos en un chat, el titulo del chat te indica sobre que temas van a tratar en el mismo 😉

Excelentes funcionalidades las que tiene Slack!

Me gustaría recomendar las mejoras que Google está realizando a su herramienta [Google Chat](https://gsuite.google.com/intl/es-419/products/chat/) para ofrecer a sus usuarios una plataforma de mensajería diseñada para equipos para mejorar la productivida, colaboración y comunicación:

- Facilita el trabajo en un mismo lugar.
  Permite la colaboración de forma sencilla y eficaz con mensajes directos y conversaciones en grupo.
  Tiene salas virtuales exclusivas para alojar proyectos y conversaciones a lo largo del tiempo
  Simplifica el registro del progreso y el seguimiento de las tareas

Lo recomiendo también, para los que ya tengan una suscripción a Google Suite en sus empresas y aún no aprovechan al 100% sus beneficios.

[slack-platzi-fest-export.zip](https://drive.google.com/file/d/1KPMjsW-JVUh-OPwhy5Y49tnNPcW3xV35/view?usp=sharing)

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Guía para entender las herramientas de exportación e importación de Slack | Slack](https://slack.com/intl/es-mx/help/articles/204897248-Gu%C3%ADa-para-entender-las-herramientas-de-exportaci%C3%B3n-e-importaci%C3%B3n-de-Slack)

# 3. Dominar las funcionalidades básicas de Slack

## Qué puedes hacer en Slack: glosario de términos básicos

**Conceptos importantes:**

- Espacio de trabajo: El lugar donde se centraliza la información y tiene una URL única.
- Usuarios: Los miembros de tu equipo
- Canales: La forma en la que separamos las conversaciones (de acuerdo a proyectos, temas específicos, regiones, etc.)
- Mensajes directos: Conversaciones con una persona o con varias pero que no son de dominio público
- Status: Comunica tu disponibilidad (Ausente, en la oficina, ocupado)
- Hilo: Conversaciones anidadas dentro de un mensaje en un canal de slack.
- Menciones: Llamar la atención de un usuario etiquetándolo en la conversación (@usuario). Slack le envía una notificación al usuario para que esté enterado.

> “Manten los halagos en público y haz los llamados de atención en privado”

**Recomendaciones en el uso de Slack**

- Se recomienda conversar en canales públicos para mantener la transparencia
- Usar canales y mensajes privados solamente para temas legales/salarios/salud

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Glosario de Slack | Slack](https://slack.com/intl/es-co/help/articles/213817348-Glosario-de-Slack)

## Canales de Slack: creación, uso y configuración

**¿Qué tener en cuenta a la hora de crear un canal?**

- Tener un patron de nombres
- Agregar una descripción que defina el uso del canal
- Poner un mensaje pineado en el canal
  **¿Qué opciones tenemos en los canales**
- Puede ser público o privado
- Ver la conversación sin entrar al mismo
- Agregar el canal a favoritos (No tener más de 10)
- Restringir/gestionar permisos de publicación
  **Tips finales**
- Es aconsejable tener un canal de conversaciones informales
- Establecer un código de conducta que rija la comunicación

<img src="https://i.ibb.co/M1cPSJ6/organizado.jpg" alt="organizado" border="0">

> Slack es lo más, definitivamente: muy útil, te permite organizar tus conversaciones en canales públicos, conversar a través de hilos, se puede conectar a otras aplicaciones, etc. ¡Muy recomendado!

Un canal para gestionar las necesidades de capacitación de mi equipo de trabajo y motivar la cultura de aprendizaje.

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[¿Qué es un canal? | Slack](https://slack.com/intl/es-co/help/articles/360017938993-%C2%BFQu%C3%A9-es-un-canal)

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Cómo fijar mensajes | Slack](https://slack.com/intl/es-es/help/articles/205239997-C%C3%B3mo-fijar-mensajes)

## Mensajes y notificaciones de Slack

Por defecto el corrector de ortografía (Spellcheck) está en ingles, en preferencias se puede editar o agregar un nuevo idioma de escritura.

Excelente el truco de las palabras clave. En mi caso, por ahora, sería: “help”, “urgente”.
📢Las notificaciones globales coinciden con las de Discord, ¡que maravilla! Pero ahora sé el alcance de cada uno. Pequeño aporte:

**Menciones**: al editar un mensaje, y hacer una mención, no se generará la notificación.
Existen notificaciones globales: canal (channel), aquí (here), todos (everyone).
**Aquí**: dentro del canal y online dentro de la app de escritorio. 👁‍🗨 Inmediatez.
**Canal**: notificación a todos los usuarios del canal, conectados o desconectados en la app de escritorio o móvil. No hay necesidad de inmediatez. ✌
**Todos**: solo disponible para el canal principal. La madre de todas las notificaciones. Anuncios realmente importantes. No se puede abusar de este. 💥

> **Hay que tener respeto por el tiempo de los demás.**

> *Como usuario solo puedes eliminar los mensajes que hayan sido escritos por ti. Para eliminar mensajes de otras personas, debes ser administrador*

Muy útil eso de los dos puntos “:” para escribir un emoji, y los atajos para usar negrillas:
1.- Seleccionar y usar el botón “B” abajo de la caja.
2.- Seleccionar la palabra y usar “crt” + “+” + “b”.
3.- Escribir la palabra entre asteriscos.

Es muy claro el video y las Palabras claves desde mi rol en la organización configure @Reclamos @Woxi

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Gestionar los permisos para la edición y eliminación de mensajes | Slack](https://slack.com/intl/es-es/help/articles/115004868646-Gestionar-los-permisos-para-la-edici%C3%B3n-y-eliminaci%C3%B3n-de-mensajes-)

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Guía sobre las notificaciones en la computadora | Slack](https://slack.com/intl/es-co/help/articles/201355156-Gu%C3%ADa-sobre-las-notificaciones-en-la-computadora)

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Guía sobre las notificaciones en dispositivos móviles | Slack](https://slack.com/intl/es-co/help/articles/360025446073-Gu%C3%ADa-sobre-las-notificaciones-en-dispositivos-m%C3%B3viles)

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Cómo notificar a un canal o espacio de trabajo | Slack](https://slack.com/intl/es-co/help/articles/202009646-C%C3%B3mo-notificar-a-un-canal-o-espacio-de-trabajo)

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Cómo crear un grupo de usuarios | Slack](https://slack.com/intl/es-co/help/articles/212906697-C%C3%B3mo-crear-un-grupo-de-usuarios)

## Emojis como forma de comunicación

Los emojis como forma de comunicación incrementan y construyen la cultura de la empresa

<img src="https://i.ibb.co/TbdFfJL/emoji.jpg" alt="emoji" border="0">

### ¿Saben que son los metaemojis?

Es una combinación de emojis que signifique más que la suma de sus partes y se pueda escribir en bloc de notas.

Curioso, ¿verdad? Me llamo mucho la atención el análisis que hizo Ter sobre emojis en espacios 3D. Les dejo el enlace a su video, espero que les guste:

- [Creando ESPACIOS 3D con emojis: Metaemojis](https://www.youtube.com/watch?v=KPfDNpzrHlQ)

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Cómo agregar emojis personalizados | Slack](https://slack.com/intl/es-co/help/articles/206870177-C%C3%B3mo-agregar-emojis-personalizados)

![img](https://www.google.com/s2/favicons?domain=https://www.polly.ai/hubfs/favicon-1.ico)[Creating Polls in Slack](https://www.polly.ai/help/slack/creating-polls)

> Polly es un plugin para Slack y sirve para hacer encuestas



# 4. Llevar Slack al siguiente nivel con herramientas avanzadas

## Comparte enlaces, código y archivos en Slack

Para adjuntar una archivo en Slack, también se puede arrastrar el elemento en cualquier parte del canal no solamente en la caja de texto.

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Cómo agregar archivos a Slack | Slack](https://slack.com/intl/es-co/help/articles/201330736-C%C3%B3mo-agregar-archivos-en-Slack-C%C3%B3mo-agregar-archivos-en-Slack)

![img](https://www.google.com/s2/favicons?domain=https://i.forbesimg.com/48X48-F.png)[Slack Set To Accelerate From Coronavirus And Work-From-Home](https://www.forbes.com/sites/bethkindig/2020/04/26/slack-set-to-accelerate-from-coronavirus-and-work-from-home/#4df8e1cc46ba)

## Llamadas y videollamadas en Slack

Aunque la comunicación escrita de tu equipo sea óptima, en muchas ocasiones no existirá un sustituto para una llamada o videollamada. La opción de hacer llamadas de voz se introdujo a Slack como característica nativa en 2016. Pocos meses después se incorporó la opción de video y al siguiente año la de compartir pantalla.

La ventaja de hacer llamadas por Slack es que no tienes que abrir una nueva ventana ni iniciar sesión en otro servicio. El feature está justo ahí y solo necesitas asegurarte de que tu interlocutor está conectado y disponible para iniciar la llamada con un clic. Por otro lado, entre las desventajas encontramos que la calidad del sonido y el video no es la mejor cuando se compara con otros servicios similares; las videollamadas grupales y la opción de compartir pantalla son una característica exclusiva de las versiones pagas de Slack y no es posible invitar usuarios ajenos al espacio de trabajo a las llamadas grupales.

Para iniciar una llamada en Slack debes ir a la conversación privada con el usuario y hacer clic en el ícono de llamada que encuentras tanto en la parte superior de la pantalla como en el perfil del usuario.

<img src="https://i.ibb.co/rfcJ7nF/call.jpg" alt="call" border="0">

Al hacerle clic a este botón se abrirá una ventana emergente donde inmediatamente empezará a establecerse una conexión con el otro usuario. Una vez que se inicia la llamada, la ventana se verá así:

<img src="https://i.ibb.co/MpCsxBD/call1.jpg" alt="call1" border="0">

En los botones de abajo encontramos, de izquierda a derecha, las siguientes opciones: silenciar o activar tu micrófono, deshabilitar o habilitar tu cámara, compartir pantalla (esta característica estará deshabilitada si utilizas la versión gratuita de Slack), enviar una reacción y finalizar la llamada.

El botón de “enviar reacción” a su vez despliega un menú con diferentes opciones para comunicarnos con nuestro interlocutor ya sea mediante un emoji o un mensaje escrito:

<img src="https://i.ibb.co/d4nbKD1/call2.jpg" alt="call2" border="0">

En la esquina superior izquierda de la ventana encuentras otros dos botones: el primero te permite acceder a los ajustes de la llamada para establecer un nombre y configurar tus ajustes de audio y video. El segundo botón es para invitar a más usuarios a la llamada, sin embargo, esta opción solo está disponible si cuentas con una versión paga de Slack.

<img src="https://i.ibb.co/r2M6q1H/call3.jpg" alt="call3" border="0">

Puedes minimizar esta ventana emergente para consultar información ya sea en Slack u otra ventana de tu computador. Al hacer esto, la ventana automáticamente se superpondrá a la esquina superior derecha de tu pantalla, así:

<img src="https://i.ibb.co/P5kRwLW/call4.jpg" alt="call4" border="0">

Además, si usas la versión paga de Slack puedes iniciar llamadas dentro de canales públicos o privados. La llamada aparecerá dentro del canal para que cualquier miembro pueda unirse. El botón para crear una llamada dentro de un canal lo encuentras en el menú de información del canal:

<img src="https://i.ibb.co/HrRvKNL/call5.jpg" alt="call5" border="0">

Ahora ya sabes cómo usar la característica de llamadas y videollamadas en Slack para comunicarte con tu equipo de trabajo. Sabes iniciar una llamada y navegar a través de las diferentes opciones que te ofrece la interfaz. Si tienes alguna duda adicional te recomiendo consultar [este artículo](https://slack.com/intl/es-co/help/articles/115003498363-Slack-Calls--conceptos-básicos) de la sección de ayuda de Slack.

## Configura notificaciones y recordatorios en Slack

Para agendar un recordatorio en Slack se utiliza la siguiente sintaxis:

```bash
/remind [@alguien | #canal] [que] [cuando]
```

------

Teniendo eso en cuenta, podríamos recordarnos algo recurrente de la siguiente forma:

```bash
/remind me Do Code Review every weekday at 9am
```

De esta forma Slackbot nos recordará todos los días entre la semana (Lunes a Viernes) a las 9 de la mañana de hacer Code Review.

------

Si queremos recordar a alguien mas en nuestro Workspace debemos reemplazar el `me` por el handle de la persona, de la siguiente forma:

```shell
/remind @elonamuskera Buy domain elonamuskera.com tomorrow at 9am
```

De esta forma Slackbot le enviará un recordatorio al usuario con el handle @elonamuskera el día de mañana a las 9 de la mañana.

Para agendar un recordatorio a otra persona:

```bash
/recordar @miguel comprar comida para el evento hoy a las 5 pm
```

Para un recordatorio a un canal:

```bash
/recordar a #festival-platzi “deadline para confirmar ponentes mañana a las 7 pm
```

Recordatorios recurrentes:

```bash
/recordar ver ponentes confirmados todos los días a las 10 am
```

> “Tu eres dueño de tu tiempo, no de tus notificaciones”
>
> La función de recordar es de las mejores. El slackbot es una gran ayuda.

```bash
/recordar @persona o #canal
Súper útil!
```

> /recordar [@mariandreacruz] [Ya aprendí a hacer recordatorios a otros usuarios] [En 20 minutos]

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Cómo pausar tus notificaciones a través del modo No molestar | Slack](https://slack.com/intl/es-co/help/articles/214908388-C%C3%B3mo-pausar-tus-notificaciones-a-trav%C3%A9s-del-modo-No-molestar)

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Cómo programa un recordatorio | Slack](https://slack.com/intl/es-co/help/articles/208423427-C%C3%B3mo-programa-un-recordatorio)

## Cómo realizar búsquedas en Slack

<img src="https://i.ibb.co/8Kwmt1J/buscar.png" alt="buscar" border="0">

> La búsqueda es muy buena ya que te da muchas opciones de filtrar.
>
> “Juana Alpaca” de:@mariandrea en:#memes después-de:2020-04-28 antes-de:2020-05-01
>
> Buenísimo el buscador.

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Cómo buscar en Slack | Slack](https://slack.com/intl/es-co/help/articles/202528808-C%C3%B3mo-buscar-en-Slack)

## Atajos de teclado y comandos de Slack

ATAJOS:
Para acceder a los comandos, en la barra de mensaje, dígito ** /** (y se despliega la lista con los comandos)

- /contraeer : contrae los mensajes para mejor visualización
  /ampliar: muestra los mensajes contraidos
  /dnd 15 min: habilitar no molestar por un tiempo
  /disponible: desactivar no molestar
  /estado 😃 actualiza mi estado
  /secret - Personalizado del equipo platzi para enviar mensaje secreto a los fundadores.

**¿Cuál es el comando o atajo que utilizaría para conocer la lista de todos los miembros de un canal?** se digita /quienes y luego la tecla ENTER

```bash
/aplicaciones*	Busca aplicaciones de Slack en el Directorio de Aplicaciones
/archivar	Permite archivar este canal
/ausente*	Cambia tu estado a "ausente"
/contraer	Reduce todas las imágenes y vídeos incorporados a ese canal (opuesto a /expandir)
/dnd [una hora] 	Inicia o finaliza una sesión en modo no molestar
/expandir	Expande todas las imágenes y vídeos incorporados a ese canal (opuesto a /contraer)
/feed ayuda [o subscribir, lista, retirar] 	Gestiona tus suscripciones RSS
/sugerencias [tu texto] 	Envía tus comentarios o solicita ayuda a Slack 
/invitar @alguien [#canal]	Invita a un miembro a un canal 
/unirse [#canal]	Abre un canal del que te conviertes en miembro
/salir (o /cerrar o /abandonar)	Permite abandonar un canal
/mí [tu texto]	Muestra un texto en cursiva, p. ej., /me da un paso de baile muestra da un paso de baile
/msj [#canal] (o /dm @alguien) [tu mensaje] 	
Envía un mensaje a un canal o envía un mensaje directo privado a otro miembro 

/silenciar	Desactiva las notificaciones de un canal (o vuelve a activarlas si están desactivadas). En la versión de ordenador, también puedes usar /silenciar para no seguir un hilo.
/abrir [#canal]	Abre un canal
/recordar [@alguien o #canal] [qué] [cuándo]	Configura un recordatorio para un miembro o un canal
/recordar ayuda	Obtener más información sobre cómo fijar recordatorios
/recordar lista	Muestra una lista de los recordatorios que has creado
/eliminar (o /quitar-a) @alguien	Elimina a un miembro del canal actual. Puede que esta acción esté restringida a propietarios o administradores del espacio de trabajo.
/renombrar [nombre nuevo]	Cambia el nombre de un canal (solo para administradores)
/buscar [tu texto]* 	Busca en los mensajes y archivos de Slack 
/atajos de teclado* 	Abre el cuadro de diálogo de atajos de teclado 
/encoger_hombros [tu mensaje]	Añade ¯\_(ツ)_/¯ al final de tu mensaje
/estado	Establecer o borrar tu estado
/tema [texto]	Define el tema de un canal
/quiénes	Muestra una lista de hasta 100 miembros de este canal
```

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Atajos de teclado en Slack | Slack](https://slack.com/intl/es-co/help/articles/201374536-Atajos-de-teclado-en-Slack)

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Utilizar comandos de barra diagonal integrados | Slack](https://slack.com/intl/es-co/help/articles/201259356-Utilizar-comandos-de-barra-diagonal-integrados)

## Slackbot es tu amigo: configura respuestas automáticas

> Después de un rato queriendo ver porque no funcionaba el bot, me di cuenta que no funciona para los mensajes privados, solo funciona para las preguntas en canales.
>
> El amigo bot!, interesante las funciones que tiene slack.

Maravillosa esta clase, en el trabajo solíamos usar los atajos de Slackbot con la siguiente estructura: `![keyword]`. Inmediatamente vi esta clase fui a modificar estos mensajes para hacerlos más amigables con un lenguaje más natural.

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Introducción a Slackbot | Slack](https://slack.com/intl/es-co/help/articles/202026038-Introducci%C3%B3n-a-Slackbot)

# 5. Integrar Slack con otras herramientas

## Integración de Slack con Google Drive y Google Calendar

Las integraciones facilitan el trabajo; pero lo más importante es ser muy cuidadosos con la seguridad para no tener vulnerabilidades que después se conviertan en un dolor de cabeza.
Leer bien y revisar siempre los permisos que vamos a autorizar.
Ser cuidadosos con los permisos que damos a los archivos que compartimos a través de nuestro drive y definir si se requiere permiso de edición o solo lectura. También podemos definir caducidad en la versión paga de google suite.

> Una integración bastante sencilla pero útil es con Doodle ya que permite escoger los horarios de reunión que se nos acomoden dentro del mismo chat.

![img](https://www.google.com/s2/favicons?domain=https://a.slack-edge.com/80588/marketing/img/meta/favicon-32.png)[Guía sobre las aplicaciones y el Directorio de aplicaciones de Slack | Slack](https://slack.com/intl/es-co/help/articles/360001537467-Gu%C3%ADa-sobre-las-aplicaciones-y-el-Directorio-de-aplicaciones-de-Slack)

## Integración de Slack con IFTTT

IFTTT es un servicio que nos permite programar diferentes automatizaciones a la medida de nuestras necesidades. Su nombre es una sigla de la frase “*If This, Then That*”, la cual podemos traducir como “si sucede esto, entonces haz eso”. Básicamente, nos brinda una gran gama de posibilidades para conectar diferentes herramientas, páginas, redes sociales e incluso *hardware* para automatizar acciones que realizamos con frecuencia.

En esta clase habilitaremos la integración de IFTTT con nuestro espacio de trabajo PlatziFest y luego haremos dos automatizaciones: publicar un mensaje automático en un canal cada vez que alguien escriba un *tweet* utilizando el *hashtag* del evento; y crear un “botón del pánico” para avisarle a nuestro equipo si vamos tarde.

## Integrando IFTTT con Slack

Lo primero que haremos será crear nuestra cuenta en IFTTT. Este servicio es gratuito. Para crear la cuenta ingresamos a [www.ifttt.com](https://www.ifttt.com/), aquí tenemos la opción de crear una nueva cuenta con un correo o autenticarnos con nuestra cuenta de Apple, Google o Facebook. En este caso he decidido conectar la cuenta de Google que creamos para el curso así que haré clic en “Continue with Google”.

<img src="https://i.ibb.co/kMp6cxv/f.jpg" alt="f" border="0">

na vez autenticados, vamos a ir al directorio de aplicaciones de nuestro espacio de trabajo en la barra lateral y buscaremos IFTTT. Cuando encontremos la *app* haremos clic en “Añadir”.

<img src="https://i.ibb.co/nfz2KRR/ff.jpg" alt="ff" border="0">

En la ventana que se abre en nuestro navegador haremos clic en “Añadir a Slack”.

<img src="https://i.ibb.co/NT1YSb5/ff0.jpg" alt="ff0" border="0">

Esto nos llevará a otra pestaña dentro de la página de IFTTT donde usaremos el botón “Connect” para autorizar la integración.

<img src="https://i.ibb.co/Wn9rM20/ff1.jpg" alt="ff1" border="0">

Por último, haremos clic en “Permitir” para conceder los permisos que solicita IFTTT sobre nuestra cuenta de Slack.

<img src="https://i.ibb.co/r7xzsDh/ff2.jpg" alt="ff2" border="0">

## Publicar tweets automáticamente en un canal

¡Muy bien! Ya hemos hecho la integración de IFTTT con nuestro espacio de trabajo. Ahora viene lo interesante: configuraremos IFTTT para que trabaje para nosotros y nuestro evento. Queremos monitorear activamente el hashtag #platzifest para saber qué dicen nuestros estudiantes sobre el evento en redes sociales. Qué práctico sería recibir todos estos *tweets* en un canal de nuestro espacio de trabajo en tiempo real. Para lograrlo iremos a [www.ifttt.com/slack](https://ifttt.com/slack) y entre la lista de opciones que nos ofrece IFTTT seleccionaremos “Post to Slack when a tweet matches your search term”.

<img src="https://i.ibb.co/48q85R5/ff3.jpg" alt="ff3" border="0">

En la siguiente ventana haremos clic en “Connect”.

<img src="https://i.ibb.co/CH1V54Z/ff4.jpg" alt="ff4" border="0">

Este botón nos redirigirá a Twitter, es necesario que tengas un usuario de Twitter para usar esta integración. Así que llenamos los campos de correo y contraseña de Twitter y haremos clic en “Authorize app”.

<img src="https://i.ibb.co/YpXRfkp/ff5.jpg" alt="ff5" border="0">

Una vez autenticados en Twitter, nos redirige nuevamente a IFTTT para configurar la búsqueda que queremos monitorear. Aquí debemos especificar en el primer campo cuál es la palabra clave que vamos a buscar, en nuestro caso “#platzifest”. Esta búsqueda puede ser tan refinada como queramos mediante las [herramientas de búsqueda avanzada de Twitter](https://help.twitter.com/en/using-twitter/twitter-advanced-search).

A continuación debemos especificar dónde queremos que aparezcan los resultados, puede ser en un canal, en una conversación privada o en un grupo privado de usuarios. Para nuestro caso seleccionamos “Channels”. Al hacerlo, en el siguiente campo se cargará la lista de canales públicos de nuestro espacio de trabajo, vamos a seleccionar el canal “social-media”. A continuación damos clic en “Save” y con esto ya quedará lista y funcionando esta integración.

<img src="https://i.ibb.co/0n8L9BD/ff6.jpg" alt="ff6" border="0">

Así es como se verán los tweets que recibiremos en el canal #social-media en tiempo real gracias a la integración con IFTTT. El mensaje en Slack incluso nos ofrece un link por si queremos ir a la publicación original.

<img src="https://i.ibb.co/qsDWYJC/ff7.jpg" alt="ff7" border="0">

## Botón del pánico

Ahora vamos a agregar una automatización más que nos permitirá crear un “botón del pánico” para avisarle a nuestro equipo cuando por alguna razón estemos yendo tarde hacia un compromiso de trabajo. Para usar esta integración es necesario que tengamos instalada la [aplicación de IFTTT](https://play.google.com/store/apps/details?id=com.ifttt.ifttt) en nuestro teléfono e iniciemos sesión con la cuenta que creamos en el paso 1.

Iremos nuevamente a [www.ifttt.com/slack](https://ifttt.com/slack) y esta vez seleccionaremos la opción “Quickly let a Slack channel know you’re running late”.

<img src="https://i.ibb.co/FJp5Tjw/ff8.jpg" alt="ff8" border="0">

A continuación haremos clic en “Connect”, tal como lo hicimos con la automatización anterior. La siguiente ventana nos avisa que debemos tener la última versión de la aplicación para usar esta automatización. Nuevamente haremos clic en “Connect”.

<img src="https://i.ibb.co/W024j4c/ff9.jpg" alt="ff9" border="0">

El siguiente paso es configurar la automatización. En primer lugar seleccionaremos dónde se publicará este mensaje automático, en mi caso seleccioné el canal #platzifest. Luego nos permite personalizar el mensaje que se enviará cuando presionemos el botón del pánico. En mi caso me he decidido por un mensaje que muestre que estoy muy apenada con mi equipo por el retraso y que sabré compensarlos con unas ricas donas al llegar a la oficina. Nota que he usado el nombre de un emoji encerrado entre dos puntos. De esta forma, cuando se publique el mensaje en Slack, aparecerá con el emoji que escogí.

Por último, podemos darle un título a nuestro mensaje, este campo es opcional. Una vez hemos llenado los campos necesarios, haremos clic en “Save”.

<img src="https://i.ibb.co/4SQGGFY/ff10.jpg" alt="ff10" border="0">

Ahora que configuramos nuestro botón del pánico es necesario habilitarlo en nuestro teléfono. Vamos a ir a la aplicación de IFTTT y, si no lo hemos hecho, iniciar sesión con nuestro usuario. La pantalla principal nos muestra las dos automatizaciones que tenemos hasta ahora. Vamos a seleccionar la que acabamos de configurar.

<img src="https://i.ibb.co/p27YVxZ/ff11.jpg" alt="ff11" border="0">

A continuación iremos a las opciones de configuración seleccionando el engranaje en la esquina superior derecha.

<img src="https://i.ibb.co/6yFmLrV/ff12.jpg" alt="ff12" border="0">

Y haremos clic en “Add” para agregar un acceso directo a este botón en nuestra pantalla principal.

<img src="https://i.ibb.co/vP2kGmr/ff13.jpg" alt="ff13" border="0">

Con esto, quedará agregado el botón del pánico en nuestro teléfono. ¡Úsalo solo en caso de emergencia! Al presionar el botón no nos pedirá ningún tipo de confirmación, simplemente enviará el mensaje al canal que escogimos.

<img src="https://i.ibb.co/Vp8gZKZ/ff14.jpg" alt="ff14" border="0">





<img src="https://i.ibb.co/JzzdMsQ/ff15.jpg" alt="ff15" border="0">

Como puedes ver, las posibilidades de IFTTT y de las aplicaciones integradas a Slack son muchísimas. Te invito a seguir explorando esta herramienta, así como el [directorio de aplicaciones de Slack](https://slack.com/apps), para encontrar esas integraciones perfectas para las necesidades de tu espacio de trabajo.

> “Tutoriales de estudiantes” sobre cómo [conectar Slack a Zapier](https://platzi.com/tutoriales/1951-slack/6142-integrar-zapier-con-slack/) por si es de su interés ☺️.

# 6. Cierre del curso

## Felicitaciones por completar el curso

@mariandrea Mil gracias por este excelente curso. He aprendido mucho y me encantó la metodología y la energía que inspiras a través de tus clases.

- Me gustan los cursos que combinan clases en video con las clases sólo escritas (sobre todo para paso a paso un poco complejos).

También los enlaces y las memorias que nos compartes por la pestaña de “Archivos y enlaces” nos permiten ir más allá para nunca parar de aprender.

A futuro sería ideal si puedes agregar en la parte de descripción el resumen con los puntos clave que hablas en cada clase. Esto también complementa perfecto el aprendizaje y sobre todo nos ayuda a repasar los conceptos clave (un día después, una semana después y un mes después)

