<h1>Engineering Management</h1>

<h3>Juan Pablo Buriticá</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [Lo que aprenderás sobre engineering management](#lo-que-aprenderás-sobre-engineering-management)
  - [¿Este curso de engineering management es para ti?](#este-curso-de-engineering-management-es-para-ti)
  - [Quién debería aprender engineering management](#quién-debería-aprender-engineering-management)
  - [Roles en organizaciones de ingeniería](#roles-en-organizaciones-de-ingeniería)
  - [Trayectorias de carrera en equipos técnicos](#trayectorias-de-carrera-en-equipos-técnicos)
  - [Niveles en organizaciones de ingeniería](#niveles-en-organizaciones-de-ingeniería)
  - [Camino técnico](#camino-técnico)
  - [Camino gerencial](#camino-gerencial)
- [2. Saltando a la Gerencia](#2-saltando-a-la-gerencia)
  - [¿Estás lista para ser gerente?](#estás-lista-para-ser-gerente)
  - [Tú como líder técnica](#tú-como-líder-técnica)
  - [Gerente o no gerente](#gerente-o-no-gerente)
- [3. Primeros 30 días cómo gerente](#3-primeros-30-días-cómo-gerente)
  - [Primeros 30 días: ¿Qué hago aquí?](#primeros-30-días-qué-hago-aquí)
  - [Conociendo a tu equipo](#conociendo-a-tu-equipo)
  - [1:1](#11)
- [4. Días 30 a 60](#4-días-30-a-60)
  - [Delega](#delega)
  - [Conectando estrategia y ejecución](#conectando-estrategia-y-ejecución)
  - [Manejo de estado de trabajo y de equipos](#manejo-de-estado-de-trabajo-y-de-equipos)
  - [Días 1 a 60: ¿Cómo te sientes?](#días-1-a-60-cómo-te-sientes)
  - [El tiempo de ingeniería](#el-tiempo-de-ingeniería)
- [5. Tips para una gerencia eficiente](#5-tips-para-una-gerencia-eficiente)
  - [Sé la trabajadora que te gustaría tener](#sé-la-trabajadora-que-te-gustaría-tener)
  - [Desarrollo de producto](#desarrollo-de-producto)
  - [Itera rápido y ágil](#itera-rápido-y-ágil)
  - [¿Qué es velocidad en ingeniería?](#qué-es-velocidad-en-ingeniería)
  - [Estructurando a tu equipo](#estructurando-a-tu-equipo)
  - [Estrategias de comunicación](#estrategias-de-comunicación)
  - [¿Cómo tomar decisiones?](#cómo-tomar-decisiones)
  - [Demo: RFCs](#demo-rfcs)
  - [Performance, contratación y siguientes](#performance-contratación-y-siguientes)


# 1. Introducción

## Lo que aprenderás sobre engineering management

Tecnicas modernas para dirigir a tu equipo.

## ¿Este curso de engineering management es para ti?

**Empatía**
Capacidad para ponerse en el lugar del otro y entender lo que siente o piensa.

Solo vamos a usar el termino de gerente de ingenieria.

La gerencia no es una ciencia exacta, cuestiona, evalua y ve si funciona.

- El contexto local es importante, porque tu industria depende del ambiente. EVALUA EL AMBIENTE LOCAL Y VE QUE PROCESO TE PUEDE AYUDAR. ***Hay procesos que no funcionan a tu pequeña empresa.\***
- El contexto corporativo en industrias grandes, starup con procesos rigidos. NO INTENTES NADAR RIO ARRIBA.
- Empatia = Colocarse en los zapatos de la otra persona.

> La GERENCIA no es una ciencia exacta. TODO lo que aprendas DEBES cuestionarlo, evaluarlo y darle tu propio estilo.

[Business-Management.pdf](https://drive.google.com/file/d/1G09aPaqGj5L66oaLNgDEbWdBBnDnW4LN/view?usp=sharing)

## Quién debería aprender engineering management

El CTO, Chief Technology Officer o director de tecnología es el máximo responsable del departamento técnico y tecnológico de una compañía.

La práctica para ser Gerente de Ingeniería de Software es compleja, esto quiere decir que hay mucha tarea pendiente

> NUNCA debería haber gerentes de ingeniería, por ejemplo con un equipo de programadores, que no sepa lo que hacen o cómo lo hacen.
>
> Para llegar a ser Gerente de cualquier área sería fundamental tener el conocimiento y experiencia en realizar los procesos y el trabajo desde su cimiento.

## Roles en organizaciones de ingeniería

Ing de Software -> Project Manager
Ing de Software -> Lider técnico
Ing de Software -> 🚀 (aún no sé esta parte)
Gracias por usar femenino en este curso, es un detallito, pero se siente genial escucharlo y sentir que es para mí.

**Operan en dominios Técnicos:**

- Ingeniera de Software: Esta encargada de las implementaciones tecnicas.

- Líder Técnica: Coordina el estado de la aplicación.

- Arquitecta: Responsable de como construir el software hacia el futuro.

  **Operan en el dominio de personal, proyecto o negocio:**

- Gerente de Ingeniería: Influencia al software a través de personas

- Gerente de Proyecto: Esta a cargo de la coordinación del proyecto.

- Gerente de Producto: Que debemos hacer y porque?

**Roles.**

- Ingeniero de Software: Dominio técnico - responsable de implementaciones técnicas, escribir código.
- Líder Técnico: Dominio técnico - mira mas adelante negociación
- Arquitecto: Dominio técnico - mira el futuro como mantener el software en el tiempo y aplicable a actualizaciones
- Gerente de Ingeniería: Dominio de personas, técnico - a cargo de personal técnico
- Gerente de Proyecto: Dominio de personas, proyectos
- Gerente de producto: Dominio de personas, negocio

## Trayectorias de carrera en equipos técnicos

Un líder es responsable. Sabe que su liderazgo le da poder, y utiliza ese poder en beneficio de todos.
Un líder esta informado. Se ha hecho evidente que en ninguna compañía puede sobrevivir sin líderes que entiendan o sepan cómo se maneja la información. Un líder debe saber cómo se procesa la información, interpretarla inteligentemente y utilizarla en la forma más moderna y creativa.

**En ingeniería tenemos dos caminos.**

- Camino técnico
  - Te especializas en ejecución técnica
  - Autoridad sobre dominio técnico
  - Autoridad sobre tu tiempo
  - No estás a cargo de personas
- Camino gerencial
  - Te especializas en gerencia de personal técnico
  - Poder de decisión
  - Tu impacto técnico es a través de personas
  - Estás a cargo de personas

## Niveles en organizaciones de ingeniería

- IC levels = Individual Contributor (No estas a cargo de personal)
- Manager Levels

Comparación de los niveles de ingenieros en LinkedIn, Microsoft y Amazon.

![img](https://qph.fs.quoracdn.net/main-qimg-305658f3a754792d09c0a4a0749bfdb0)

Ejemplos de niveles:

- Splice
- Rent The Runway
- Levels.fyi

> Muy importante poder trazar una carrera de un junior … identificar para que es bueno y orientarlos.
> Muchas personas no trabajan a gusto por qué no saben que quieren y los líderes no saben ayudarles tampoco.

[![img](https://www.google.com/s2/favicons?domain=//ssl.gstatic.com/docs/spreadsheets/favicon3.ico)Public Copy of Splice Eng Levels 2018-12-21 - Google Sheets](https://bit.ly/splice-levels)

[![img](https://www.google.com/s2/favicons?domain=//ssl.gstatic.com/docs/spreadsheets/favicon3.ico)Software Development/Leadership Ladder, multi-track, public - Google Sheets](https://docs.google.com/spreadsheets/d/1k4sO6pyCl_YYnf0PAXSBcX776rNcTjSOqDxZ5SDty-4/edit)

[![img](https://www.google.com/s2/favicons?domain=https://www.levels.fyi/assets/favicon.png)Levels.fyi - Compare career levels across companies](https://www.levels.fyi/)

## Camino técnico

I love The Ten Commandments of Egoless Programming. But I think we can adjust them and apply them in everything in you personal and work life. The ten commandments are shown below:

- **Understand and accept that you will make mistakes**.
- **You are not your code**.
- **No matter how much “karate” you know, someone else will always know more**.
- **Don’t rewrite code without consultation**.
- **Treat people who know less than you with respect, deference, and patience**.
- **The only constant in the world is change**.
- **The only true authority stems from knowledge, not from position**.
- **Fight for what you believe, but gracefully accept defeat**.
- **Don’t be “the coder in the corner.**
- **Critique code instead of people**

![2020-04-20_18-58.png](https://static.platzi.com/media/user_upload/2020-04-20_18-58-9d2b7ade-aede-4804-a902-1d5b54fd7d1b.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://www.kitchensoap.com/wp-content/uploads/2014/08/cropped-image-1-32x32.jpg)Kitchen Soap – On Being A Senior Engineer](https://www.kitchensoap.com/2012/10/25/on-being-a-senior-engineer/)

[![img](https://www.google.com/s2/favicons?domain=https://blog.dbsmasher.com/2019/01/28/on-being-a-principal-engineer.html/assets/images/favicon.png)On Being A Principal Engineer](https://blog.dbsmasher.com/2019/01/28/on-being-a-principal-engineer.html)

## Camino gerencial

**Saber delegar:**
Un líder no puede hacer todo solo. Pensar esto, es algo irrealista. Por lo que un buen líder necesita dejar solos y confiar en sus colaboradores para que el trabajo en equipo sea eficiente.

**Incentivar una buena comunicación:**
La falta de comunicación es la responsable del fracaso de muchas operaciones en las empresas. Así que incentivar una de utilidad es una de las maneras más sensatas de cultivar el éxito.

**Inspirar al grupo:**
Un grupo estará desanimado si se encuentra frente a la falta de motivación. El rol del líder está diseñado en parte, para inspirar y transmitir pasión a sus colaboradores.

- Tu éxito llega a través de tu equipo y no a través de ti misma
- Es responsable del crecimiento de su equipo y sus individuos
- Tiene impacto indirecto en la tecnología, no puedes sentarte a escribir código
- Debe esperar para ver el resultado de su trabajo

<h3>Liderazgo vs gerencia</h3>

- Para liderar no necesitas un título
- Para liderar no necesitas permiso
- Ser gerente no implica que seas líder
- La gerente tiene autoridad, la líder influencia

> 💥 Lo ideal es ser gerente y al mismo tiempo líder para tener mejor resultados

La agencia de un gerente es muy diferente al de una ingeniera.

De *On Being A Principal Engineer* me encantó esto:
*"…But once i became a principal…i now carried a larger impact on morale, **organization culture **and even on recruiting and representing my engineering organization outside of the company…"*

Siendo un líder e incluso manager, somos capaces de modificar a menor o mayor escala el ADN de la compañía a través de valores (o anti valores) lo cual se refleja en la cultura organizacional.

Totalmente de acuerdo en que el éxito de todo el team es el de 1 manager y viceversa. Precisamente por esto es que creo que todo manager debería estar llamado (casi obligatoriamente) a ser 1 líder.

![2020-04-20_19-05.png](https://static.platzi.com/media/user_upload/2020-04-20_19-05-e215d9eb-db0e-449b-8c06-d6d8c754a979.jpg)

# 2. Saltando a la Gerencia

## ¿Estás lista para ser gerente?

Un gerente debe tener dos prerequisitos antes de convertirse en uno:

- Líder técnica

- Mentora

  Este ultimo, puede incrementar tu impacto ayudando a que otros crezcan, Aprendes a escuchar y estar presente, a delegar y escalarte, a que te comprendan y ofrecerle tu tiempo a otras personas.

Mi mejor mentor a sido técnico de electrónica de mi primer trabajo, me enseño a ser mas minuciosos en las cosas y no creer en todo lo que dice la gente.

**Al ser Gerente tienes:**

- Oportunidad de ser mentor
- Oportunidad de ser lider técnico

## Tú como líder técnica

**Gerencia de Proyectos:**

- desglosa el trabajo
- Empuja a pesar de la ambigüedad
- Ajusta el plan a medida que avanzan
- Adapta los requerimientos de acuerdo a las nueva información.
- Antes de terminar. re-evalúa detalles.

> El trabajo profesional de **ingeniería de software** es **construir negocios a través de tecnología**, y no simplemente crear tecnología. –_Juan Pablo Buriticá_.

![2020-04-20_19-22.png](https://static.platzi.com/media/user_upload/2020-04-20_19-22-9e6b74d5-7966-492d-aa2b-1e13797da240.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://www.levels.fyi/assets/favicon.png)Highest Paying Tech Companies of 2018 by Levels.fyi](https://www.levels.fyi/2018/)

## Gerente o no gerente

> “La cultura del geek alfa puede ser muy dañina a la colaboración y afectar negativamente a quienes no les queda tan fácil pelear por si mismos.”

![2020-04-20_19-38.png](https://static.platzi.com/media/user_upload/2020-04-20_19-38-c2ad273e-8f20-4eab-aa15-d4418fe1f2c3.jpg)

- Crees que la autoridad soluciona todos los problemas.
- No tienes más opciones de crecimiento en tu trabajo actual.
- Buscas estatus en un título o posición
- Te consideras la más inteligente y que por eso debes estar a cargo.

Geeks Alpha:
Valoran la inteligencia y habilidad técnica sobre otras habilidades, y piensan que éstos determinan quién tomas las decisiones.

- Son personas efectivas en proyectos técnicos
- Valoran esfuerzos individuales a grupales
- Tienen opiniones rígidas sobre soluciones
- Se les dificulta tomar dirección de personas que no respetan intelectualmente

Si sospechas que eres una geek alfa, prueba a ser mentora. Puede que se te dificulte ser gerente, toma un pausa y ayuda a otros a crecer.

Si no estás dispuesta a cambiar, no tomes este rol ni sigas el camino de la gerencia. Ya que puedes hacer daño.

Para ser buena gerente requiere que seas consciente de tus propios límites, para que puedas crecer y trabajar en fallas.

> 🚀 Ser gerente no es una promoción, no vas a saltar hacia arriba sino a hacia el lado.

<h4>Razones incorrectas para tomar el salto a gerencia</h4>

- Crees que la autoridad soluciona todos los problemas
- No tienes más opciones de crecimiento en tu trabajo actual
- Buscas estatus en un título o posición
- Te consideras la más inteligente y que por eso debes estar a cargo

> 😉 Es importante que en nuestras empresas creemos estructuras donde es fácil fallar y volver a donde estábamos.

# 3. Primeros 30 días cómo gerente

## Primeros 30 días: ¿Qué hago aquí?

![2020-04-20_19-46.png](https://static.platzi.com/media/user_upload/2020-04-20_19-46-c42adfb1-5515-4e28-aae6-c4be32810e3e.jpg)

**Eres responsable de:**
\- Desarrollo personal y profesional de tu equipo
\- Rendimiento individual y grupal
\- Desbloquear, proteger y motivar
\- Reclutar ingenieros
\- Implementar y adaptar procesos
\- Comunicar interna y externamente
\- Conectar la estrategia empresarial con la ejecución de tu equipo
\- Fomentar buenas decisiones
\- La calidad del trabajo de tu equipo

Eres la voz de tu equipo, no los compromentas

Como gerente en que **NO** serás responsable:

- Tomar todas las decisiones
- Supervisar cada hora de tus reportes
- Revisar cada línea de código que escribe tu equipo. Puedes participar pero no es tu trabajo.
- Escribir código

## Conociendo a tu equipo

- Que **le** ofreces **tu** **a** **la** **la** gerencia, **al** equipo **y** **a** **la** empresa. 

- ¿Cuáles son tus actividades favoritas? 

- ¿Cuá**l** es **tu** mayor fortaleza **y** debilidad?

  ![2020-04-20_19-57.png](https://static.platzi.com/media/user_upload/2020-04-20_19-57-08792fdd-dc34-46a5-8a2e-f629a72c0de1.jpg)

  **Conociendo a tu equipo**

  - Establece confianza
  - primer 1:1:

[![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)mgt/primer-uno-a-uno.md at master · buritica/mgt · GitHub](https://github.com/buritica/mgt/blob/master/es/primer-uno-a-uno.md)

## 1:1

![2020-04-20_20-07.png](https://static.platzi.com/media/user_upload/2020-04-20_20-07-f3ee3ab4-5560-4552-bfce-8e85fa21e557.jpg)

**¿Qué son las reuniones uno a uno?**
La reunión uno a uno tienen las siguientes características:

- Son sesiones entre 2 personas
- Son recurrentes, se repiten con cierta frecuencia, yo diría que el mínimo sería una vez al mes
- Pueden ser programados o bien a petición puntual de la otra persona
- Debe haber un entorno seguro para que la otra persona esté cómoda tratando cualquier tema profesional
- Son de duración corta pero variable, yo diría que entre 5 y 30 minutos
  **Como objetivos de las reuniones uno a uno podemos tener:**
- Tener una idea de en qué estado emocional y laboral está cada persona del equipo
- Dar y recibir feedback personalizado
- Detectar posibles puntos de mejora personales

**1:1**
Tu principal herramienta

Objetivos:

- Alineamiento y sincronización
- Establecer una buena relación
- Desbloquear o dar contexto
- Guiar o dirigir decisiones
- Atención a tu equipo

Tipos

- Agenda predeterminada
- Pongámonos al día
- Sesión de retroalimentación
- Cómo vamos con ….

- Conozcámonos

# 4. Días 30 a 60

## Delega

![2020-04-20_20-15.png](https://static.platzi.com/media/user_upload/2020-04-20_20-15-3c269ccc-26dd-422a-bce4-ff35086da84a.jpg)

- La responsabilidad no implica que hagas todo el trabajo, esto significa que si eres ingeniera no tienes
- Explica el resultado que esperas claramente
- Haz preguntas que confirmen tu claridad
- Acepta que te va a tomar más tiempo inicialmente
- Tareas que no te gusten a ti tiene valor para otras personas

> 🚗 Entrega tus legos. Dale tareas a tu equipo grandes para que aprendan.

## Conectando estrategia y ejecución

- Estrategia

   

  → Vendría siendo cuál es el objetivo final de nuestra empresa, o sea que esta es como nuestra guía. Existe varios tipos de estrategia como:

  - Negocio
  - Producto
  - Técnicas
  - Ingeniería

- **Ejecución** → Son los pasos que vamos tomando que nos van ha acercar a nuestro destino final.

> 🔨 Nuestro rol en estos casos es conectar nuestro destino final con el trabajo localizado de ejecución que está logrando nuestro equipo.

Debes empezar a entender cómo construir puentes con otros departamentos.

[![img](https://www.google.com/s2/favicons?domain=https://www.cnbc.com/2018/04/18/why-elon-musk-wants-his-employees-to-use-a-strategy-called-first-principles.html/favicon.ico)Why Elon Musk wants his employees to use a strategy called 'first principles'](https://www.cnbc.com/2018/04/18/why-elon-musk-wants-his-employees-to-use-a-strategy-called-first-principles.html)

[![img](https://www.google.com/s2/favicons?domain=https://cdn-static-1.medium.com/_/fp/icons/favicon-rebrand-medium.3Y6xpZ-0FSdWDnPM3hSBIA.ico)Elon Musks’ “3-Step” First Principles Thinking: How to Think and Solve Difficult Problems Like a Genius](https://medium.com/the-mission/elon-musks-3-step-first-principles-thinking-how-to-think-and-solve-difficult-problems-like-a-ba1e73a9f6c0)

## Manejo de estado de trabajo y de equipos

1. Establece un plan (Durante los próximos 15 días) y enséñale a tu equipo a que te informe cuando hay cambios en el plan.
2. No crees trabajo innecesario para reportarte.
3. Crea un ritmo como el plan del punto uno.
4. Utiliza canales adecuados. (Comunica la información del estado que te llega, hacia arriba y hacia los lados.)
   .

## Como mantener al día

- Sienta expectativas al comienzo.
- Comunica cambios en el plan.
- Comunica riesgos tan pronto como surja.
- Usa los canales adecuados
- Establece un ritmo.

No micro-gerencies: Dale espacio a tu equipo de responder, a que falle, con calma.
.
No seas la jefe a la que nadie quiere darle malas noticias. **Cuando recibas malas noticias, sé neutral o positiva.**

> En la sección de “Usa herramientas” seria interesante que se recomiende que herramientas se pueden utilizar desde la experiencia del profesor… que administrador de proyectos?, que herramientas de monitoreo? Etc.

![2020-04-20_21-37.png](https://static.platzi.com/media/user_upload/2020-04-20_21-37-06bb8f86-8d49-4a6b-83df-d51b18880a6c.jpg)

>  Github es una excelente herramienta que puede ser muy útil para programadores y equipos de IT.

> ✨ ¿Cómo vamos?

Esta es la pregunta que todos los gerentes nos hacen. Esto pasa porque cuando somos responsables del trabajo de varias personas, es importante entender cómo van en trabajo de una manera fácil.

**Cómo estar al día**

- Enseña a tu equipo a que te informe cuando hay cambios en el plan
- No crees trabajo innecesario para reportarte
- Establece un ritmo
- Usa canales adecuados

Es importante comunicar el estado en que te llega hacia arriba y hacia los lados en la organización.

**Cómo mantener al día**

- Sienta expectativas al comienzo
- Comunica cambios en el plan
- Comunica riesgo tan pronto como surja
- Usa canales adecuados
- Establece un ritmo
- Usa herramientas

> No micro-gerencies 🔥

Tampoco seas esta persona a la que nadie quiere darle malas noticias. Cuando recibas malas noticias se neutral o positivo. Y recuerda apoyar a tu equipo.

En **situaciones críticas** todo este ritmo debemos acelerarlo un poco, si se cae algo o deja de funcionar algunas cosas crea un canal de comunicación sobre la emergencia, y comienza a dar updates en un tiempo de intervalos.

**Guía de estado**

- ¿Qué objetivos lograste esta semana (o en este periodo)?
- ¿Qué objetivos logró tu equipo esta semana?
- ¿Hay algún riesgo que creas que yo deba estar al tanto?
- ¿Qué proyecto o iniciativa de la próxima semana te motiva?

![management.JPG](https://static.platzi.com/media/user_upload/management-919d458f-b9e2-42eb-9d9f-0524ba0ed10c.jpg)

## Días 1 a 60: ¿Cómo te sientes?

![2020-04-20_21-41.png](https://static.platzi.com/media/user_upload/2020-04-20_21-41-57811b3d-2948-48c8-b0d4-1057589c8d24.jpg)

> ✨ Toma el control

Tal vez a este punto te sientas cansado, a sido muy difícil y creas que las decisiones que has tomado toma estén erradas, pero tranquilo vas por buen camino.

**Recomendación***😗 Cada semana por los Lunes en la mañana empieza con tres objetivos que tengas para la semana.

Esto te dará un sentido de progreso y record histórico de las cosas que has logrado.

[![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)mgt/guia-estado.md at master · buritica/mgt · GitHub](http://bit.ly/guia-de-estado)

## El tiempo de ingeniería

![2020-04-20_22-06.png](https://static.platzi.com/media/user_upload/2020-04-20_22-06-a4a464c1-18ac-45b1-848d-e06bfc7b65f8.jpg)

El valor del tiempo de ingeniería es uno de los recursos más valiosos que tenemos.

> El tiempo es $$$ 💸

Por ejemplo, podemos categorizar el tiempo así:

- **Soporte** → Es este tiempo que no planeamos que necesitamos reaccionar.
- **Mantenimiento** → Es el tiempo que dedicamos a mejorar o mantener nuestros sistemas.
- **Desarrollo de nueva funcionalidad** → Este tiempo que invertimos en desarrollar nuevos features o productos para el software.

El tiempo de desarrollo es muy limitado. Tenemos que aprender a balancear.

Funcionalidad nueva → 70%

Soporte y mantenimiento → 30%

**Comprende el tiempo**

- Mide la inversión del tiempo
- Usa las herramientas disponibles
- Contar personas es más fácil que contar horas

Es más fácil contar gente, que horas. Así que utiliza gente para que pueda mantener el Soporte y Mantenimiento.

> 💢 No poder desarrollar producto es un riesgo grande para la empresa.

**Forma una estrategia**

- Estabiliza los sistemas
- Comprende
- Mejora
- Escala

> ☁ Ejecuta tu estrategia e itera

Y por último, es comunicar el progreso. Tienes que hacer ver cuál es el impacto que está creando este mantenimiento y mejora.

> Categorización del tiempo del equipo de desarrollo: - Soporte. - Mantenimiento. - Desarrollo de funcionalidad.

# 5. Tips para una gerencia eficiente

## Sé la trabajadora que te gustaría tener

Conexiones, o redes de contactos es vital para nuestro rol.

![2020-04-20_22-18.png](https://static.platzi.com/media/user_upload/2020-04-20_22-18-c6a4d4e2-e7d9-496f-98c0-c79331d22168.jpg)

> 🤭 Busca ser la trabajadora que te gustaría tener

Para esto tienes que hacerle la vida fácil a las personas que estás a cargo.

Así que:

- Crea una agenda y compártela antes
- Llega a tiempo
- Pregunta sobre el negocio
- Pregúntale a tu jefa que le preocupa
- Establece una relación

> 🙌 Todo esto se reduce a **ayuda a que te ayuden**

- Si te estás quejando, aclara si necesitas ayuda o solo un oído. Siempre que te quieras quejar llévaselo a gente en tu mismo nivel o a tu jefe y aclara si necesitas ayuda o solo que te escuchen.
- Si buscas ayuda, trae sugerencias
- Pide **conexiones** para poder acortar esta diferencia de conocimiento que tienes tú con tu equipo
- Hazle la vida fácil a tu jefa

> 🔨 No olvides mantener a tu jefa al día

## Desarrollo de producto

Podemos tender a dividir el desarrollo en dos partes:

- Desarrollo de producto
  - Este es cuando desarrollamos tecnología nueva usando hipótesis, design thinking, estrategia de experiencia de usuario para resolver problemas muy puntuales a través de productos y servicios.
  - Podemos estar haciendo trabajo nunca antes hecho
  - Estimados son inciertos
  - Resultados tienen alta variabilidad
- Entrega de producto
  - Permitir el flujo rápido de desarrollo a producción, disminuyendo la variabilidad y el tamaño del trabajo
  - Integración, pruebas y despliegue deben realizarse de manera continua y rápida
  - Los tiempos de ciclo son conocidos y predecibles
  - Resultados tienen baja variabilidad

> ✨ El desarrollo de producto es asociación

y la entrega de producto es responsabilidad de ingeniería…

El concepto de agilidad nace en el 2001 cuando 20 ingenieros escribieron un manifiesto de 12 principios de cómo entregar software de mejor calidad, cómo ser menos burocráticos y pensar menos en procesos y valorar más **productos funcionales.**

![2020-04-20_22-39.png](https://static.platzi.com/media/user_upload/2020-04-20_22-39-6fa17abb-ad0c-402c-9058-f42ee57c69c9.jpg)

**Principios**:

1. Nuestra mayor prioridad es satisfacer al cliente mediante la entrega temprana y continua de ‘software’ con valor.
2. Aceptamos que los requisitos cambien, incluso en etapas tardías del desarrollo. Los procesos ágiles aprovechan el cambio para proporcionar ventaja competitiva al cliente.
3. Entregamos ‘software’ funcional frecuentemente, entre dos semanas y dos meses, preferentemente en el periodo de tiempo más corto posible.
4. Los responsables de negocio y los desarrolladores trabajamos juntos de forma cotidiana durante todo el proyecto.
5. Los proyectos se desarrollan en torno a individuos motivados. Hay que darles el entorno y el apoyo que necesitan, y confiarles la ejecución del trabajo.
6. El método más eficiente y efectivo de comunicar información al equipo de desarrollo, y entre los miembros del equipo, es la conversación cara a cara.
7. El ‘software’ funcionando es la medida principal de progreso.
8. Los procesos ágiles promueven el desarrollo sostenible. Los promotores, desarrolladores y usuarios debemos ser capaces de mantener un ritmo constante de forma indefinida.
9. La atención continua a la excelencia técnica y al buen diseño mejora la agilidad.
10. La simplicidad, o el arte de maximizar la cantidad de trabajo no realizado, es esencial.
11. Las mejores arquitecturas, requisitos y diseños emergen de equipos autoorganizados.
12. A intervalos regulares el equipo reflexiona sobre cómo ser más efectivo para a continuación ajustar y perfeccionar su comportamiento en consecuencia.

## Itera rápido y ágil

El concepto de agilidad nace en el 2001 cuando 17 ingenieros escribieron un manifiesto de 12 principios de cómo entregar software de mejor calidad, cómo ser menos burocráticos y pensar menos en procesos y valorar más **productos funcionales.**

Este manifiesto se llama 📖 “*agile manifesto*”.

Para trabajar de una manera ágil podemos:

- Un ritmo estable y predecible es importante
- Enseña a tus ingenieras desglosar su trabajo y trabajar en cambios pequeños.
- Las pruebas unitarias son herramientas de diseño y monitoreo, no de aceptación
- Invierte en automatización, tanto despliegue como pruebas
- la calidad es responsabilidad de todos
- Instrumenta y monitorea los sistemas
- Desacopla el despliegue del lanzamiento
- Despliega diariamente, con confianza

> 🙌 Adapta tus procesos y no te obsesiones con ellos

### [manifiesto ágil completo](https://agilemanifesto.org/iso/es/manifesto.html):

Estamos descubriendo formas mejores de desarrollar software tanto por nuestra propia experiencia como ayudando a terceros. A través de este trabajo hemos aprendido a valorar:

- **Individuos e interacciones** sobre procesos y herramientas

- **Software funcionando** sobre documentación extensiva

- **Colaboración con el cliente** sobre negociación contractual

- **Respuesta ante el cambio** sobre seguir un plan

  Esto es, aunque valoramos los elementos de la derecha, valoramos más los de la izquierda.

### [Principios del Manifiesto Ágil](https://www.bbva.com/es/agile-manifiesto-que-es/)

Estos cuatro valores se concretan en 12 principios, que definen el marco de trabajo de cualquier equipo ágil:

1. Nuestra mayor prioridad es satisfacer al cliente mediante la entrega temprana y continua de ‘software’ con valor.
2. Aceptamos que los requisitos cambien, incluso en etapas tardías del desarrollo. Los procesos ágiles aprovechan el cambio para proporcionar ventaja competitiva al cliente.
3. Entregamos ‘software’ funcional frecuentemente, entre dos semanas y dos meses, preferentemente en el periodo de tiempo más corto posible.
4. Los responsables de negocio y los desarrolladores trabajamos juntos de forma cotidiana durante todo el proyecto.
5. Los proyectos se desarrollan en torno a individuos motivados. Hay que darles el entorno y el apoyo que necesitan, y confiarles la ejecución del trabajo.
6. El método más eficiente y efectivo de comunicar información al equipo de desarrollo, y entre los miembros del equipo, es la conversación cara a cara.
7. El ‘software’ funcionando es la medida principal de progreso.
8. Los procesos ágiles promueven el desarrollo sostenible. Los promotores, desarrolladores y usuarios debemos ser capaces de mantener un ritmo constante de forma indefinida.
9. La atención continua a la excelencia técnica y al buen diseño mejora la agilidad.
10. La simplicidad, o el arte de maximizar la cantidad de trabajo no realizado, es esencial.
11. Las mejores arquitecturas, requisitos y diseños emergen de equipos autoorganizados.
12. A intervalos regulares el equipo reflexiona sobre cómo ser más efectivo para a continuación ajustar y perfeccionar su comportamiento en consecuencia.

## ¿Qué es velocidad en ingeniería?

Cuando hablamos de velocidad nos referimos a la práctica de estimar tareas de ingeniería, ya sea puntos o tamaños de camisetas, sumarlas y intentar entender cuánto vamos a poder hacer una determinado tiempo.

> 🔥🔥🔥 Velocidad como métrica de productividad es problemático.

La velocidad es relativa y depende de cada equipo. Y cuando se usa como métrica de productividad los equipos ‘juegan’ la métrica y esto afecta la colaboración.

Si usas capacidad como una métrica de productividad, haces que tu equipo opere de una forma insostenible y no pueden reaccionar a cambios.

![2020-04-20_22-55.png](https://static.platzi.com/media/user_upload/2020-04-20_22-55-9ab66786-5527-490d-a416-10b24b9e0faa.jpg)

> “No usaré la velocidad como métrica para mi equipo”

## Estructurando a tu equipo

![2020-04-20_23-03.png](https://static.platzi.com/media/user_upload/2020-04-20_23-03-9fcf40a2-406b-4d41-a527-7be80c87a73b.jpg)

**Especialidades**: 

- Engineering Management
- Devops Engineer
- Backend Engineer
- Frontend Engineer
- DataScientist
- QA / Tester

✨ **Consejos:**

- No crees barreras imaginarias
- Enfócate en dar objetivos claros, y no en decidir cómo se soluciona el problema
- Balancea la ejecución inmediata con el crecimiento a largo plazo
- Rota las responsabilidades (con soporte podemos rotar)

> 🔥 La individualidad impacta el rendimiento y conocimiento.

## Estrategias de comunicación

Ahora como tu trabajo es comunicarte con persona y no dispositivos, debes aprender a comunicarte muy bien.

Hay dos tipos de comunicación:

- Sincrónica
  - Chat
  - Llamadas
- Asincrónica
  - Emails

Para una buena comunicación debemos utilizar los canales correctos.

- Control de versiones
- Organizador de tareas
- Chat
- Foro
- Emails
- Calendaro

No crees una prisión de presencia, no utilices esto para medir el trabajo de tu equipo, así que no te enfoques en ver si alguien está conectado o no.

> 💕 La comunicación es el fundamento de la confianza.
>
> No crees una prisión de presencia

![2020-04-20_23-10.png](https://static.platzi.com/media/user_upload/2020-04-20_23-10-cf6e62dc-8096-4945-a568-3d9f13ff4ec4.jpg)

## ¿Cómo tomar decisiones?

Estar a cargo implica tomar decisiones. Recuerda que **No tomar una decisión es mucho peor que una mala decisión.**

> 🚩 RFCs como mecanismos de decisión de equipos.

RFCs significa ***Request for comments\*** son documentos colaborativos, una propuesta que le hace una persona ciertos miembros del equipo sobre la dirección que quieren tomar. Como wireframes que te ayudan a bosquejar una solución.

- Un espacio de pregunta y discusión
- Menos rígidos que el código
- Documentos colaborativos que ayudan a alinear y entender
- Espacios de aprendizaje
- Cunas de experimentación
- Herramientas de empoderamiento
- Documentos históricos

Pero tienes que tener en cuenta que los RFCs no son:

- Documentos que buscan consenso, no busca que la decisión la tomes tú, al menos que esto perjudique a la empresa debes intervenir
- Espacios para criticar
- Solicitudes de permiso
- Exclusivamente para discusiones técnicas.

>  “Equivocarse” no está mal
> Lo que está mal es no aceptar los errores ni querer mejorar.

"Una mala decisión es mejor que no decidir 💚"

## Demo: RFCs

<h3>Título (lee el source code del markdown para ver los comentarios)</h3>

Autores:

- @githubusername

#### 1 TL;DR

<!--
párrafo corto que explica qué estas proponiendo
-->

#### 2 Motivación

<!--
¿qué motiva esta decisión y por qué es importante?
el propósito de esta sección es articular de una manera sencilla el valor de la d3ecision que vamos a tomar
-->

#### 3 Propuesta de implementación

<!–
Este es el núcleo de tu propuesta, y su proposito es ayudarte a pensar en la solución. Esto debe ser un wireframe, no un documento perfecto con todos los detalles.

Escribir es pensar https://medium.learningbyshipping.com/writing-is-thinking-an-annotated-twitter-thread-2a75fe07fade

- usa diagramas para ilustrat tus ideas o flujos
- inluye ejemplos de código si estas proponiendo una interfaz o contrato de sistemsa nuevo
- agrega links con las especificaciones de proyectos

El proposito de esta sección se resume en:
"Esta es la dirección en la que nos voy a llevar, alguién ve huecos en mi propuesta o tiene comentarios sobre cómo mejorarla?

–>

#### 4 Métricas

<!--
Que métricas debemos vamos a instrumentar, o monitorear para observar las implicaciónes de esta decisiòn?
Por ejemplo, cuando interactuamos con un sistema externo que tipo de latencia esperariamos o si agregamos una tabla nueva que tan rápido se llenaría?
-->

#### 5 Riesgos e inconvenientes

<!--
¿Hay razones por las que no deberiamos hacer esto?
¿Qué riesgos estamos tomando? Por ejemplo, no tenemos experiencia con esta tecnología nueva o no entendemos la escala aún.
-->

#### 6 Alternativas

<!--
¿Hay otras formas de resolver éste problema?
-->

#### 7 Impacto potencial y dependencias

<!–
¿Qué otros sistemas se verán afectados con esta propuesta?
¿Qué consideraciones de seguridad debemos tener?¿Como pueden explotar esta parte del sistema?
¿Que impacto tiene esta decision sobre soporte al cliente?

Aquí buscamos ser concientes del ambiente en el que operamos y generar empatía hacia otros que pueden verse afectados por nuestra decisión.
–>

#### 8 Preguntas sin resolver

<!--
¿ Qué preguntas no hemos resuelto?
-->

#### 9 Conclusión

#### 10 El proceso (elimina esta sección)

<!–
Al escribir un RFC, estas incluyendo al equipo en la dirección que estas tomando. En muchos casos puede haber multiples soluciones, y tambien opiniones diferentes sobre como atacar un problema. Es posible que en el futuro esta propuesta no sea la mejor solución posible, pero aprenderemos de ella.

Como proponente, estas tomando responsabilidad sobre la dirección que quieres tomar y con este documento buscas que tus otros miembros de nuestro equipo contribuyan con sus comentarios acerca de tu idea, pero ultimamente esta decision es tuya y te apoyamos.

En resumen, este documento es:

- un ejercicio de pensamiento, prototipamos con palabras
- un record historico, y su valor puede disminuir con el tiempo
- un mecanismo para
- una forma de transmitir información
- un mecanismo para construir confianza
- una herramienta de empoderamiento
- un canal de comunicación

Este documento no es

- una solucitud de permiso
- un documento que requiere aprobación
- la representación actual de nuestros sistemas o procesos
- 

–>

- [ ] Copia este template
- [ ] Bosqueja el documento, piensa que es un wireframe en prosa
- [ ] Compartelo con personas de tu equipo para retroalimentación inicial
- [ ] Envíalo como un pull request
- [ ] Etiquétalo para que sea facil categorizarlo
- [ ] Compartelo con todas las personas a quien les pueda interesar
- [ ] Comunica un limite de tiempo razonable dependiendo de la complejidad de la decisión
- [ ] Pidele a dos personas que entiendan el probelma que lo revisen por tí, o pidele ayuda a tu manager
- [ ] Hazle merge con dos +1

<h3>Recomendaciones</h3>

- Utiliza la etiqueta [WIP] si aún estas refinando detalles
- Utiliza la etiqueta [newbie] si tienes una propuesta en la que tienes poca confianza por tu conocimiento actual
- Si hay areas específicas en las que quieres atencion, etiqueta a personas que consideras que saben algo al respecto y preguntales directamente. “María, impacto crees que va a tener este API sobre tu base de datos?”
- Si tienes dudas, pídele ayuda a tu manager o lider de tecnología
- Es tu decisión
- Ten en cuenta la prioridad de las propuestas que estas haciendo, los RFC no son documentos para proponer rearquitecturas o proyectos “cool” que no se alinean con los objetivos a corto plazo de la empresa

[![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)mgt/guia-de-rfcs.md at master · buritica/mgt · GitHub](https://github.com/buritica/mgt/blob/master/es/guia-de-rfcs.md)

## Performance, contratación y siguientes

![2020-04-20_23-55.png](https://static.platzi.com/media/user_upload/2020-04-20_23-55-9cd9e6de-d36c-43e5-8320-4e085829bcf0.jpg)



