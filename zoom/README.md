<h1></h1>


<h3></h3>


<h1>Tabla de Contenido</h1>

- [1. ¿Qué necesito para empezar?](#1-qué-necesito-para-empezar)
  - [¿Qué es Zoom?](#qué-es-zoom)
  - [Requisitos del sistema](#requisitos-del-sistema)
  - [Accesorios útiles para tus videollamadas](#accesorios-útiles-para-tus-videollamadas)
- [2. Cómo instalar Zoom](#2-cómo-instalar-zoom)
  - [Cómo instalar Zoom en el teléfono](#cómo-instalar-zoom-en-el-teléfono)
- [3. Mi primera videoconferencia en Zoom](#3-mi-primera-videoconferencia-en-zoom)
  - [Ingresar a una reunión (móvil y escritorio)](#ingresar-a-una-reunión-móvil-y-escritorio)
  - [Crear y programar una reunión](#crear-y-programar-una-reunión)
  - [Recorrido por las herramientas](#recorrido-por-las-herramientas)
  - [Compartir pantalla y grabar](#compartir-pantalla-y-grabar)
- [4. Dudas y mejores prácticas](#4-dudas-y-mejores-prácticas)
  - [Buenas prácticas para videollamadas](#buenas-prácticas-para-videollamadas)
  - [Tips y trucos para usar en Zoom](#tips-y-trucos-para-usar-en-zoom)
  - [Resolución de problemas frecuentes](#resolución-de-problemas-frecuentes)
  - [Diferentes usos específicos que puedes tener al usar Zoom](#diferentes-usos-específicos-que-puedes-tener-al-usar-zoom)
  - [Adaptando el contenido para transmitirlo en zoom](#adaptando-el-contenido-para-transmitirlo-en-zoom)
  - [Despedida](#despedida)
- [5. Introducción](#5-introducción)
  - [Introducción a las herramientas de Webinars de Zoom y Seguridad para usar la aplicación.](#introducción-a-las-herramientas-de-webinars-de-zoom-y-seguridad-para-usar-la-aplicación)
- [6. Zoom Webinars](#6-zoom-webinars)
  - [Características y Beneficios](#características-y-beneficios)
  - [Conoce herramientas de Zoom Webinars](#conoce-herramientas-de-zoom-webinars)
  - [Utilizando Templates y Configuraciones en Zoom Webinars](#utilizando-templates-y-configuraciones-en-zoom-webinars)
  - [Personzalizar la Marca con Zoom Webinar](#personzalizar-la-marca-con-zoom-webinar)
  - [Reportes y grabaciones](#reportes-y-grabaciones)
- [7. Seguridad y Privacidad en Zoom](#7-seguridad-y-privacidad-en-zoom)
  - [Contexto y casos](#contexto-y-casos)
  - [Consejos para usar Zoom de forma segura](#consejos-para-usar-zoom-de-forma-segura)
- [8. Recapitulación](#8-recapitulación)
  - [Recapitulación](#recapitulación)

# 1. ¿Qué necesito para empezar?

## ¿Qué es Zoom?

Zoom es la herramienta que uso en este tiempo de confinamiento. Quiero aprender a usarlo con todas sus posibilidades.

- [slides-zoom.pdf](https://drive.google.com/file/d/1ZrR6qIIi4_8zah-naX057P0Hh2D7-SuL/view?usp=sharing)

- Zoom es una herramienta que nos permite hacer videollamadas en tiempo real, principalmente para empresas,ahora por la situación se usa en grupos de amigos y familiares.
- Tiene opción gratuita y otra de paga.
- Video y audio en HD
- Tiene herramientas de colaboración para participar.
- Tiene un chat para mensajes en tiempo real.
- Permite grabaciones y transcripciones.
- Uso de calendario
- Chat escrito.

## Requisitos del sistema

![](https://i.ibb.co/gjg4D3K/zoom1.jpg)

![](https://i.ibb.co/m5Jzgk3/zoom.jpg)

## Accesorios útiles para tus videollamadas

![](https://i.ibb.co/rZ0Frpw/acceso-zoom.jpg)

# 2. Cómo instalar Zoom

## Cómo instalar Zoom en el teléfono

**Instalación de Zoom en Debian (Gnu/Linux)**

1. Descargo desde la página principal

- [Download Center - Zoom](https://zoom.us/download)

![](https://i.ibb.co/rtDCj9B/zoom1-1.jpg)

2. Descarga completa

![](https://i.ibb.co/sghZfFQ/zoom1-2.jpg)

3. Ejecuto este comando en la terminal para instarlo

```bash
$ sudo dpkg -i zoom_amd64.deb
```

4. Listo ya podemos usar Zoom en el sistema operativo Debian

![](https://i.ibb.co/HxkyGXC/zoom1-3.jpg)

**Instalar zoom en macOS**

![](https://i.ibb.co/rMQqcmt/zoom-mac.jpg)

# 3. Mi primera videoconferencia en Zoom

## Ingresar a una reunión (móvil y escritorio)

El proceso para instalar Zoom en dispositivos móviles es mucho más rápido que hacerlo en una computadora. A continuación te enseñaré en 3 sencillos pasos cómo realizar este proceso dependiendo del sistema operativo que tengas en tu celular.

Antes de proceder a la instalación, recuerda echarle un ojo a [la documentación oficial de Zoom](https://support.zoom.us/hc/en-us/articles/201179966-System-Requirements-for-iOS-iPadOS-and-Android) donde especifican los requisitos mínimos con los que debe contar tu teléfono para poder utilizar correctamente la aplicación.

**Para Android**

1. Vas a dirigirte a la tienda de aplicaciones de Android: Google Play Store
2. En el buscador de la tienda vas a escribir Zoom
3. Te van a aparecer MUCHAS opciones, pero vas a presionar el botón “Instalar” de la primera opción: Zoom Cloud Meetings 😉

![](https://i.ibb.co/NSCVZx3/zoom-mobil.jpg)

4. ¡Listo! Ya habrás instalado la aplicación en tu dispositivo Android

![](https://i.ibb.co/dtKVK81/zoom-mobil1.jpg)

**Para iOS**

Aquí el proceso no cambia mucho, es casi el mismo.

1. Vas a dirigirte a la tienda de aplicaciones de iOS: App Store
2. En el buscador de la tienda vas a escribir Zoom
3. Te van a aparecer MUCHAS opciones, pero vas a presionar el botón “Instalar” (sí, el ícono que tiene la nube con una flecha hacia abajo) de la primera opción: Zoom Cloud Meetings 😉 (¿Déjà vu?)

![](https://i.ibb.co/Xj1d5Lt/zoom-mobile-2.jpg)

4. ¡Listo! Ya habrás instalado la aplicación en tu dispositivo iOS

No te preocupes sobre cómo iniciar sesión o cómo usar la versión móvil de Zoom, todo esto se explicará más adelante en el curso.

Si no lograste instalar Zoom en tu dispositivo, avísanos en la sección de discusiones (acá abajo 👇🏻). Te ayudaremos para que puedas solucionar el problema que te esté sucediendo.

## Crear y programar una reunión

**Ingresar a una reunión: (como invitado)**

  - Lo primero que necesitamos es un ID
  - Ese ID lo recibimos por e-mail, por mensaje o un enlace web
  - Al enlace le damos click o lo podemos abrir en algún navegador


- Investigando un poco sobre truco que se mencionó en el video para el “+” en la cuenta de correo, encontré que es una forma de crear alias para tener cuentas de correo infinitas. La forma es la siguiente

- Si tu cuenta es tunombre@gmail.com, podemos conseguir nuevas cuentas tipo tunombre+trabajo@gmail.com o tunombre+1@gmail.com, y generar tantos alias como necesites.

- Y hablamos de alias porque estas variaciones de tu cuenta de Gmail no son cuentas como tal, aunque pueden servirte de forma similar, ya que recibirás todos los correos en tu cuenta de siempre (tunombre@gmail.com).

- Estas cuentas de Gmail no te servirán para enviar correo; de hecho, no puedes iniciar sesión con ellas. Es mucho más sencillo que eso. Lo que sí puedes hacer es utilizar esas direcciones para registrarte en distintas plataformas o a la hora de darle tu correo a alguien.

- Los correos que te envíen a estos alias te seguirán llegando a tu cuenta original.


- [Video Conferencing, Web Conferencing, Webinars, Screen Sharing - Zoom](https://zoom.us/)

## Recorrido por las herramientas

Pueden silenciar a todos con un solo click, tomen nota de ello. tener la versión mas reciente de zoom para tener acceso a los nuevos botones.

## Compartir pantalla y grabar

la documentación de Zoom, estos son los formatos en los que graba:

- **MP4:** Formato de audio/video que usa la grabación de Zoom. Por defecto, se denomina «zoom_0.mp4». Cada grabación subsiguiente se encuentra en orden secuencial: zoom_0, zoom_1, zoom_2, …zoom_x
- **M4A:** formato de audio que usa el archivo de solo audio. Por defecto, se denomina «audio_only.m4a». Cada grabación subsiguiente se encuentra en orden secuencial: audio_only_0,audio_only_1, …audio_only_x
- **M3U:** archivo de lista de reproducción para reproducir/cargar todos los archivos MP4 individuales (solo en Windows)
- **Chat:** texto estándar o archivo .txt. Por defecto, se denomina «chat.txt».

Aquí puedes encontrar más información: https://support.zoom.us/hc/es/articles/201362473-Grabación-local. 😉

Puede grabarla alguien más pero debe habilitar esa opción el host. Te paso este enlace donde lo explican mejor https://support.zoom.us/hc/en-us/articles/201362473-Local-recording#h_1f7fc5e7-7f6d-42bd-a87a-aa1c186e33af

# 4. Dudas y mejores prácticas

## Buenas prácticas para videollamadas

![](https://i.ibb.co/0nzMGY4/buenas-practicas.webp)

## Tips y trucos para usar en Zoom

![](https://i.ibb.co/BcdpJdP/tips.webp)

## Resolución de problemas frecuentes

Zoom recomienda 2.5mbps (up/down) para llamadas grupales.

Hay que tener en cuenta que Zoom no se actualiza por sí mismo sino que hay que hacerlo manualmente.

Los errores no permiten una comunicación efectiva, los ruidos generan desorden en una reunión virtual.

- [FAST](https://fast.com/es/) también muestra la velocidad de subida haciendo click en el botón Ver más información.
- Este es otro link para medir la velocidad [testmy](https://testmy.net/)
- [SPPEDTEST](https://www.speedtest.net/es)


## Diferentes usos específicos que puedes tener al usar Zoom

**Puedes utilizar ZOOM para:**

- Trabajar desde casa
- Dar talleres de diferentes tipos
- Dar clases online
- Reuniones con familiares y amigos

Reuniones de trabajo, familiares por motivos de aislamiento fuera del centro del país. Para dar charlas y capacitaciones a colegas y ex colegas de trabajo. Para conversar con amistades, jugar y por qué no. Para compartir algunas recetas y tips secretos.

**Asistencia remota:**

Puedes manejar remotamente el escritorio de un cliente para brindarle asistencia. Con ello zoom puede ser una alternativa a VNC, teamviewer o a Any Desk pues tus clientes que nos son tech ya están familiarizados con zoom. En una urgencia te salva.

## Adaptando el contenido para transmitirlo en zoom

![](https://i.ibb.co/3pzd4fj/adaptando.jpg)

## Despedida

Las mejores herramientas de teletrabajo Zoom, Slack y Trello.

> Nunca pares de Aprender.

# 5. Introducción

## Introducción a las herramientas de Webinars de Zoom y Seguridad para usar la aplicación.

**Zoom Webinars**
- Es una herramienta que nos permite crear eventos
- La comunicación es unidireccional con los participantes: Significa que es como si fueras un ponente de la conferencia. Los participantes no podrán interactuar mucho. Esa es al gran diferencia con el zoom normal.
- [zoom/webinars](https://zoom.us/webinar)

# 6. Zoom Webinars

## Características y Beneficios

Al agregar el título del webinar es recomendable que sea descriptivo y atractivo para que mas personas ingresen a la sesión.

Para tener la opción de Webinar, es necesario que el administrador de la cuenta habilite esta opción.
- **Unidireccional:** Al ser una comferencia carece de preguntas y respuestas, pero gracias a ello ayuda al ponente a tener seguridad en sus eventos
- **Posibilidad de tener mas de 1 anfitrion(host):** Gracias a ello los ponentes pueden comparir pantalla, audio o video e interactuar entre ellos
- **Pantalla solo para ponentes**
- **Capacidad para 10 y 10000 personas:** Se recomienda de 500 personas
- **Posibilidad de publicar en vivo a Youtube y Facebook:**
- **Sesion de practica:** Es un espacio(sala de espera), donde puedes interactuar con otros ponentes sin la necesidad de que otras personas vean sus conversaciones
- **Duracion:** Se recomienda como buena practica agregar 30 min demas a los webinars
- **Beneficios del Registro.**

## Conoce herramientas de Zoom Webinars

- **Registro:** Es importante debido a que, con ello podemos tener un mejor control de las personas que ingresan y no sufrir un zoombombing(Intrusion no deseada)
Se recomienda el registro de una sola vez, si las personas son recurrentes
- **Webinar Passcode:** Consiste en una proteccion adicional de 6 digitos numericos.
- **Audio:** Brinda la opcion a los participantes de conectarse por el Telefono o el Audio de la Computadora, ademas se puede selecionar desde que paises se va a conectar los participantes cuando se conectan desde el telefono para que sea llamada local
- **webinar Options:** Se recomienda activar
- **Q&A** 
- **Enable Practice Session** 
- **Require authentication to join:** Sign in to Zoom
- **Automatically record webinar in the cloud** 
- **Alternative Hosts:** Permite agregar a otro host por el correo de la organizacion
 
No lo olviden el passcode es numérico y de 6 dígitos.

## Utilizando Templates y Configuraciones en Zoom Webinars

- Despues de grabar el evento Zoom te permite grabarlo como un template
- Asimismo te permite enviar invitaciones hacia los correos de los panelistas
- Ademas se tiene la opcion de habilitar si deseas que los participantes se conecten desde diferentes dispositivos
- Importante tener en cuenta estos Templates

- Despues de grabar el evento Zoom te permite grabarlo como un template
- Asimismo te permite enviar invitaciones hacia los correos de los panelistas
- Ademas se tiene la opcion de habilitar si deseas que los participantes se conecten desde diferentes dispositivos

- [Zoom Video Webinars](https://zoom.us/webinar)

## Personzalizar la Marca con Zoom Webinar

**Puedes personalizar el correo**

- Cambiar la informacion de contacto
- Editar la informacion de confirmacion
Branding
- Es posible agregar un banner de la organizacion con un max de 1280px by 1280px
- Tambien es posible agregar un logo de max 600px by 600px
- Asimismo añadir informacion de los ponentes y agregar una foto de ellos max 400px by 400px
- Poll/Survey:Permite realizar un cuestionario de preguntas y respuestas y mostralo cuando es necesario
- Q&A:Permite realizar preguntas a los ponentes, ademas se recomienda deshabilitar dicha opcion para tener un mejor control de los participantes

Recomendable realizar con antelación las encuestas del evento.

## Reportes y grabaciones

**Nota** generalmente asiste el 70% de los asistentes que se registran.

# 7. Seguridad y Privacidad en Zoom

## Contexto y casos

**Zoombombing:** Personas no deseadas que acceden a tu reunion e interactuan de diferentes formas interrumpiendo el flujo de la comunicacion
- Utilizando la pizarra de Zoom
- Que utilicen su audio e interrumpa o via video proyectando algo que no deseen que proyecten
- Se recomienda habilitar las murallas(seguridad) para que no afecte la experiencia de los participantes

**Consejos para usar Zoom de forma segura**
- Bloquear la reunion
- Habilitar sala de espera
- Compartir pantalla
- Que se cambien el nombre
- Chatear

## Consejos para usar Zoom de forma segura

Aumentar las seguridad en todas nuestros webinars entrando a la seccion seguridad de zoom web

# 8. Recapitulación

## Recapitulación

- Como crear un evento de webinar
- Desde cuantas personas pueden tener acceso con un max de 10000 personas
- Como brocastear o compartir por redes sociales y como se puede personalizar
- Como hacer que el evento sea completamente seguro y asi evitar que terceros se incluyan e interrumpan por audio, video, chat o pizarra
- Siempre tener las cuestiones de seguridad cada vez que configures un evento

#NuncaParesDeAprender
